angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $state) {

    })
    .controller('HomeCtrl', function($scope, $state, $cordovaBarcodeScanner, $ionicModal, $ionicPopup, Monsters, $ionicLoading, $firebaseObject, store) {
        $scope.showParticles = true;

        $ionicModal.fromTemplateUrl('infomodal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openthismodal = function() {
            $scope.modal.show();
        };

        $scope.theclosebutton = function() {
            $scope.modal.hide();
        };

        $scope.monsters = Monsters;

        $ionicLoading.show({
            template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>'
        })

        var connectedRef = firebase.database().ref().child(".info/connected");
        connectedRef.on("value", function(snap) {
            if (snap.val() === true) {
                $ionicLoading.hide().then(function() {
                    $scope.offline = false
                });
            } else {
                $ionicLoading.hide().then(function() {
                    $scope.offline = true
                });
            }
        });

        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.username = fbuser;
        } else {
            $scope.username = puser;
        }

        var ref = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/planet");
        var planetRef = new $firebaseObject(ref);

        var ref2 = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/english");
        var englishref = new $firebaseObject(ref2);

        planetRef.$loaded(function() {
            $scope.myplanet2 = planetRef.$value;
            $scope.myenglish = englishref.$value;
        });

        planetRef.$watch(function() {
            $scope.myplanet2 = planetRef.$value;
            $scope.myenglish = englishref.$value
        });

        // TEMP SCAN POPUP
        // $scope.tempscan = function() {
        //  var alerteggpop = $ionicPopup.alert({
        //   template:  '<div class="image" style="background-color: #fff; background-image: url(https://cdnb.artstation.com/p/assets/images/images/004/889/023/large/beau-sullivan-icon-test-2.jpg?1487006734), url(img/skull.gif)"><h3 style="font-family: ' + $scope.benguiat +'">010010102</h3></div>',
        //   cssClass: 'eggpop animated tada',
        //   buttons: [{ text: 'Hatch' }]
        //  })
        // alerteggpop.then(function(res) {
        //   var alertPopup = $ionicPopup.alert({
        //    template:  '<div class="image" style="background-color: #fff; background-image: url(https://rampantdesigntools.com/wp-content/uploads/2014/10/monster_toolkit_small.jpg), url(img/skull.gif)"><h1 style="font-family: ' + $scope.benguiat +'">' + "George" + " " + '</h1><h3 style="font-family: ' + $scope.benguiat +'">010010102</h3></div><div class="backwhite"><ion-content><p  style="font-family: ' + $scope.Montserrat +'">' + "Bears are mammals of the family Ursidae. Bears are classified as caniforms, or doglike carnivorans, with the pinnipeds being their closest living relatives. Although only eight species of bears are extant, they are widespread, appearing in a wide variety of habitats throughout the Northern Hemisphere and partially in the Southern Hemisphere. Bears are found on the continents of North America, South America, Europe, and Asia." + '</p></ion-content><div class="glitch" data-text="Common" style="font-family: ' + $scope.Hairline +'">Common</div></div>',
        //    cssClass: 'scanpop animated pulse',
        //    buttons: [{ text: 'Catch' }]
        //   });
        // });
        // };


        $scope.scanBarcode = function() {

                $ionicLoading.show({
                    template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>'
                })

                $cordovaBarcodeScanner.scan({
                        showFlipCameraButton: false,
                        showTorchButton: true,
                        prompt: "Scan Barcode",
                        orientation: "portrait",
                        disableSuccessBeep: true
                    })
                    .then(function(barcodeData) {
                        $ionicLoading.hide().then(function() {
                            if (barcodeData.text.indexOf(".") != -1) {

                                var alertPopup = $ionicPopup.alert({
                                    template: 'QR and Advance Barcode Support coming soon.',
                                    cssClass: 'adminpop',
                                    buttons: [{
                                        text: 'Done'
                                    }, {
                                        text: 'Rescan',
                                        type: 'button-positive',
                                        onTap: function(e) {
                                            $scope.scanBarcode();
                                        }
                                    }]
                                });
                            }

                            var fbuser = store.get('fbid');
                            var puser = store.get('pwid');

                            var user = firebase.auth().currentUser;


                            if (fbuser) {
                                $scope.username = fbuser;
                                console.log("Scanning with facebook")
                            } else if(user) {
                                $scope.username = user.uid;
                                console.log("Scanning with firebase auth password")
                            }else {
                                $scope.username = puser;
                                console.log("Scanning with localhost password")
                            }

                            if (!$scope.username) {
                                console.log("USERNAME " + $scope.username)
                                var alertPopup = $ionicPopup.alert({
                                    template: 'Auth Error, please terminate the app and open it again.',
                                    cssClass: 'scanemptypop'
                                });
                            } else {
                                if (barcodeData.cancelled) {
                                    var alertPopup = $ionicPopup.alert({
                                        template: 'Cancelled. No Creatures Collected',
                                        cssClass: 'scanemptypop'
                                    });
                                } else {
                                    var rand = function(min, max) {
                                        return Math.floor(Math.random() * (max - min + 1)) + min;
                                    };

                                    var generateWeighedList = function(list, weight) {
                                        var weighed_list = [];

                                        // Loop over weights
                                        for (var i = 0; i < weight.length; i++) {
                                            var multiples = weight[i] * 100;

                                            // Loop over the list of items
                                            for (var j = 0; j < multiples; j++) {
                                                weighed_list.push(list[i]);
                                            }
                                        }

                                        return weighed_list;
                                    };

                                    var list = ['Common', 'Uncommon', 'Rare', 'Super Rare', 'Mysterious'];
                                    var weight = [0.5, 0.3, 0.1, 0.07, 0.03];
                                    var weighed_list = generateWeighedList(list, weight);
                                    var random_num = rand(0, weighed_list.length - 1);

                                    var ref = firebase.database().ref();
                                    var codesRef = ref.child("Codes");
                                    var usersRef = codesRef.child($scope.username);
                                    var monstersRef = ref.child("Monsters");

                                    monstersRef.once("value", function(snapshot) {
                                        $scope.monsters.count = snapshot.numChildren();

                                        var myarray = snapshot.val()

                                        const result = [];
                                        angular.forEach(myarray, function(val) {
                                            if (val.rarity == weighed_list[random_num]) {
                                                result.push(val.id);
                                            }
                                        });

                                        $scope.item = result[Math.floor(Math.random() * result.length) + 1];
                                    });

                                    var GetRandomMonster = Math.round((Math.random() * $scope.monsters.count) + 1);

                                    var MyMonster = firebase.database().ref().child("Monsters/" + $scope.item);

                                    $scope.benguiat = "Regular";
                                    $scope.Montserrat = "Book";
                                    $scope.Hairline = "Headlines";

                                    usersRef.child(barcodeData.text).once('value', function(snapshot) {
                                        var exists = (snapshot.val() !== null);
                                        if (snapshot.exists()) {
                                            $scope.rescan = false
                                        } else {
                                            $scope.rescan = true
                                        }
                                    });

                                    MyMonster.once("value", function(snapshot) {
                                        var data = snapshot.val();

                                        console.log(data.rarity + " " + data.catagory)

                                        if ($scope.rescan === true && data.approved === true) {

                                            if (data.collectorcount) {
                                                if (data.collectorcount == 0) {
                                                    var alertPopup = $ionicPopup.alert({
                                                        template: 'There was an error with your hunt. Please',
                                                        cssClass: 'adminpop',
                                                        buttons: [{
                                                            text: 'Done'
                                                        }, {
                                                            text: 'Rescan',
                                                            type: 'button-positive',
                                                            onTap: function(e) {
                                                                $scope.scanBarcode();
                                                            }
                                                        }]
                                                    });
                                                } else {

                                                    var objectToSave = {};
                                                    objectToSave = data.remcollectorcount - 1;

                                                    monstersRef.child(data.id).child("remcollectorcount").set(objectToSave);

                                                    if (!data.catagory || data.catagory == 'Creature') {

                                                        var isshiny = Math.floor(Math.random() * (250 - 1 + 1)) + 1;
                                                        var shinyval = Math.floor(Math.random() * (350 - 10 + 1)) + 10;

                                                        if (isshiny == 5) {
                                                            $scope.shiny = 'filter: hue-rotate(' + shinyval + 'deg)';
                                                        } else {
                                                            $scope.shiny = false;
                                                        }

                                                        usersRef.child(barcodeData.text).set({
                                                            barcode: barcodeData.text,
                                                            shiny: $scope.shiny,
                                                            user: $scope.username,
                                                            time: firebase.database.ServerValue.TIMESTAMP,
                                                            favourite: false,
                                                            Monster: data.name,
                                                            MonsterImg: data.imgurl,
                                                            MonsterDesc: data.desc,
                                                            MonsterRarity: data.rarity,
                                                            MonsterArt: data.artist,
                                                            MonsterStatus: data.approved,
                                                            CustomCss: data.CustomCss,
                                                            MonsterEnviroment: data.enviroment,
                                                            RemCollector: objectToSave,
                                                            Collector: data.collectorcount,
                                                            Catagory: 'Creature'
                                                        });

                                                        if ($scope.shiny) {
                                                            var alerteggpop = $ionicPopup.alert({
                                                                template: '<div class="image" style="background-color: #fff; background-image: url(img/egg.jpg);' + $scope.shiny + '"><h3 style="font-family: ' + $scope.benguiat + '">' + data.rarity + ' egg </h3><br /><br /><h4 style="font-family: ' + $scope.Montserrat + '">' + barcodeData.text + '</h4></div>',
                                                                cssClass: 'eggpop animated tada',
                                                                buttons: [{
                                                                    text: 'Confirm'
                                                                }]
                                                            });
                                                        } else {
                                                            var alerteggpop = $ionicPopup.alert({
                                                                template: '<div class="image" style="background-color: #fff; background-image: url(img/egg.jpg)"><h3 style="max-width="80%"; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; font-family: ' + $scope.benguiat + '">' + data.rarity + 'egg <br /><br />' + barcodeData.text + '</h3></div>',
                                                                cssClass: 'eggpop animated tada',
                                                                buttons: [{
                                                                    text: 'Confirm'
                                                                }]
                                                            });
                                                        }
                                                    } else if (data.catagory == 'Technology' || data.catagory == 'Discovery') {
                                                        usersRef.child(barcodeData.text).set({
                                                            barcode: barcodeData.text,
                                                            user: $scope.username,
                                                            time: firebase.database.ServerValue.TIMESTAMP,
                                                            favourite: false,
                                                            Monster: data.name,
                                                            MonsterImg: data.imgurl,
                                                            MonsterDesc: data.desc,
                                                            MonsterRarity: data.rarity,
                                                            MonsterArt: data.artist,
                                                            MonsterStatus: data.approved,
                                                            CustomCss: data.CustomCss,
                                                            RemCollector: objectToSave,
                                                            Collector: data.collectorcount,
                                                            Catagory: data.catagory,
                                                            Type: data.type
                                                        });

                                                        var alertPopup = $ionicPopup.alert({
                                                            template: '<div class="image" style="background-color: #fff; background-image: url( ' + data.imgurl + ' ), url(img/skull.gif);' + data.CustomCss + ';"><h1 style="font-family: ' + $scope.benguiat + '">' + data.name + " " + '</h1><h3 style="font-family: ' + $scope.benguiat + '">' + barcodeData.text + '</h3></div><div class="backwhite"><ion-content><p style="font-family: ' + $scope.Montserrat + '">' + data.desc + '</p></ion-content><div class="glitch" style="font-family: ' + $scope.Hairline + '">' + data.rarity + '</div></div>',
                                                            cssClass: 'scanpop animated pulse',
                                                            buttons: [{
                                                                text: 'Confirm'
                                                            }]
                                                        });
                                                    } else {

                                                        var alertPopup = $ionicPopup.alert({
                                                            template: 'No Creature Found',
                                                            cssClass: 'adminpop',
                                                            buttons: [{
                                                                text: 'Confirm'
                                                            }]
                                                        });
                                                    }
                                                }

                                            } else {

                                                if (!data.catagory || data.catagory == 'Creature') {

                                                    var isshiny = Math.floor(Math.random() * (250 - 1 + 1)) + 1;
                                                    var shinyval = Math.floor(Math.random() * (350 - 10 + 1)) + 10;

                                                    if (isshiny == 5) {
                                                        $scope.shiny = 'hue-rotate(' + shinyval + 'deg)';
                                                    } else {
                                                        $scope.shiny = false;
                                                    }

                                                    usersRef.child(barcodeData.text).set({
                                                        barcode: barcodeData.text,
                                                        user: $scope.username,
                                                        shiny: $scope.shiny,
                                                        time: firebase.database.ServerValue.TIMESTAMP,
                                                        favourite: false,
                                                        Monster: data.name,
                                                        MonsterImg: data.imgurl,
                                                        MonsterDesc: data.desc,
                                                        MonsterRarity: data.rarity,
                                                        MonsterArt: data.artist,
                                                        MonsterEnviroment: data.enviroment,
                                                        MonsterStatus: data.approved,
                                                        CustomCss: data.CustomCss,
                                                        Catagory: 'Creature'
                                                    });

                                                    if ($scope.shiny) {
                                                        var alerteggpop = $ionicPopup.alert({
                                                            template: '<div class="image" style="background-color: #fff; background-image: url(img/egg.jpg);' + $scope.shiny + '"><h3 style="font-family: ' + $scope.benguiat + '">' + data.rarity + ' egg </h3><br /><br /><h4 style="font-family: ' + $scope.Montserrat + '">' + barcodeData.text + '</h4></div>',
                                                            cssClass: 'eggpop animated tada',
                                                            buttons: [{
                                                                text: 'Confirm'
                                                            }]
                                                        });
                                                    } else {
                                                        var alerteggpop = $ionicPopup.alert({
                                                            template: '<div class="image" style="background-color: #fff; background-image: url(img/egg.jpg)"><h3 style="font-family: ' + $scope.benguiat + '">' + data.rarity + ' egg </h3><br /><br /><h4 style="font-family: ' + $scope.Montserrat + '">' + barcodeData.text + '</h4></div>',
                                                            cssClass: 'eggpop animated tada',
                                                            buttons: [{
                                                                text: 'Confirm'
                                                            }]
                                                        });
                                                    }
                                                } else if (data.catagory == 'Technology' || data.catagory == 'Discovery') {
                                                    usersRef.child(barcodeData.text).set({
                                                        barcode: barcodeData.text,
                                                        user: $scope.username,
                                                        time: firebase.database.ServerValue.TIMESTAMP,
                                                        favourite: false,
                                                        Monster: data.name,
                                                        MonsterImg: data.imgurl,
                                                        MonsterDesc: data.desc,
                                                        MonsterRarity: data.rarity,
                                                        MonsterArt: data.artist,
                                                        MonsterStatus: data.approved,
                                                        CustomCss: data.CustomCss,
                                                        Type: data.type,
                                                        Catagory: data.catagory
                                                    });

                                                    var alertPopup = $ionicPopup.alert({
                                                        template: '<div class="image" style="background-color: #fff; background-image: url( ' + data.imgurl + ' ), url(img/skull.gif);' + data.CustomCss + ';"><h1 style="font-family: ' + $scope.benguiat + '">' + data.name + " " + '</h1><h3 style="font-family: ' + $scope.benguiat + '">' + barcodeData.text + '</h3></div><div class="backwhite"><ion-content><p style="font-family: ' + $scope.Montserrat + '">' + data.desc + '</p></ion-content><div class="glitch" style="font-family: ' + $scope.Hairline + '">' + data.rarity + '</div></div>',
                                                        cssClass: 'scanpop animated pulse',
                                                        buttons: [{
                                                            text: 'Confirm'
                                                        }]
                                                    });
                                                } else {
                                                    var alertPopup = $ionicPopup.alert({
                                                        template: 'No Creature Found',
                                                        cssClass: 'adminpop',
                                                        buttons: [{
                                                            text: 'Confirm'
                                                        }]
                                                    });
                                                }

                                            }

                                        } else if ($scope.rescan === false) {
                                            var alertPopup = $ionicPopup.alert({
                                                template: 'You have already hunted here',
                                                cssClass: 'adminpop',
                                                buttons: [{
                                                    text: 'Done'
                                                }, {
                                                    text: 'Rescan',
                                                    type: 'button-positive',
                                                    onTap: function(e) {
                                                        $scope.scanBarcode();
                                                    }
                                                }]
                                            });
                                        } else {
                                            var alertPopup = $ionicPopup.alert({
                                                template: 'No Creature Found',
                                                cssClass: 'adminpop',
                                                buttons: [{
                                                    text: 'Done'
                                                }, {
                                                    text: 'Rescan',
                                                    type: 'button-positive',
                                                    onTap: function(e) {
                                                        $scope.scanBarcode();
                                                    }
                                                }]
                                            });
                                        }
                                    })
                                }
                            }
                        });
                    });
            },
            function(error) {
                $ionicLoading.hide().then(function() {
                    var alertPopup = $ionicPopup.alert({
                        title: 'There was a problem with your Hunt',
                        template: error,
                        cssClass: 'adminpop'
                    });
                });
                // $scope.alertPopup.close();
            };
    })



.controller('BarcodeCtrl', function($scope, $stateParams, $ionicPopup, $firebaseArray, store, $state) {

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.username = fbuser;
    } else {
        $scope.username = puser;
    }


    $scope.barloading = false

    var ref = firebase.database().ref().child("Codes/");
    var hopref = ref.child($scope.username)

    var mref = firebase.database().ref().child("Monsters");

    var results = $firebaseArray(hopref);
    var mresults = $firebaseArray(mref);

    results.$loaded(function(x) {

        var darray = [];
        $scope.mresults = mresults;

        var mresult = $scope.mresults;

        if (mresult) {
            angular.forEach(mresult, function(value, key) {
                darray.push({
                    Name: value.name,
                    ImgUrl: value.imgurl,
                    CustomCss: value.CustomCss,
                    Catagory: value.catagory,
                    id: value.id,
                    Approved: value.approved,
                    Desc: value.desc,
                    Rarity: value.rarity,
                    Enviroment: value.enviroment,
                    Type: value.type,
                    Artist: value.artist
                });
            });

            $scope.monsters = darray;

            var marray = [];
            $scope.results = results;
            $scope.barloading = true;

            var fianalresult = $scope.results;
            var lresult
            var userExists = true;

            angular.forEach(fianalresult, function(value, key) {
                marray.push({
                    Monster: value.Monster,
                    Monsterlvl: value.Monsterlvl,
                });
            });

            $scope.corrays = marray;
        }
    });

    var monstersRef = firebase.database().ref().child("Monsters");
    var allMonsters = $firebaseArray(monstersRef);

    var allMonstersArray = [];

    allMonsters.$loaded(function(x) {
        angular.forEach(allMonsters, function(value, key) {
            if (value) {
                allMonstersArray.push({
                    Name: value.name,
                    ImgUrl: value.imgurl,
                    Approved: value.approved,
                    CustomCss: value.CustomCss,
                    id: value.id
                });
            }
        });

        $scope.themonsters = allMonstersArray;
    });

    $scope.isUserMonster = function(themonster, authData) {
        var lresult
        var userExists = true;

        var looper = $scope.corrays

        angular.forEach(looper, function(value, key) {
            if (userExists) {
                if (value.Monster == themonster.Name && value.Monsterlvl) {
                    lresult = true;
                    userExists = false;
                } else {
                    lresult = false;
                }
            }
        });

        return lresult;
    }

    $scope.gotodash = function() {
        $state.go('tab.monsters')
    }

    $scope.nomonster = function(monster) {
        if (!monster.Catagory || monster.Catagory == 'Creature') {
            var alertPopup = $ionicPopup.alert({
                template: "Creature Unidentified",
                cssClass: 'adminpop'
            });
        } else if (monster.Catagory == 'Technology') {
            var alertPopup = $ionicPopup.alert({
                template: "Technology Unidentified",
                cssClass: 'adminpop'
            });
        } else if (monster.Catagory == 'Discovery') {
            var alertPopup = $ionicPopup.alert({
                template: "Discovery Unidentified",
                cssClass: 'adminpop'
            });
        } else {
            var alertPopup = $ionicPopup.alert({
                template: "Unidentified",
                cssClass: 'adminpop'
            });
        }
    };

    $scope.openModal2 = function(monster) {

        $scope.Monstername = monster.Name;
        $scope.Monsterimg = monster.ImgUrl;
        $scope.Monsterdesc = monster.Desc;
        $scope.Monsterrarity = monster.Rarity;
        $scope.Monsterenvo = monster.Enviroment;
        $scope.Monstertype = monster.Type;
        $scope.Monsterart = monster.Artist;
        $scope.CustomCss = monster.CustomCss;
        $scope.Shiny = monster.shiny;

        var alertPopup = $ionicPopup.alert({
            templateUrl: 'owned.html',
            cssClass: 'adminpop',
            scope: $scope
        });
    };
})

.controller('LoginCtrl', function($scope, $stateParams, Auth, $state, $ionicPopup, $ionicLoading, $q, store, $firebaseAuth) {

    var ref = firebase.database().ref();
    var fbuser = store.get('fbid');

    $scope.togglelogin = function() {
        $scope.userlogin = true;
        $scope.usersign = false;
    };

    $scope.togglesign = function() {
        $scope.usersign = true;
        $scope.userlogin = false;
    };

    $scope.backsign = function() {
        $scope.usersign = false;
    };

    $scope.backlog = function() {
        $scope.userlogin = false;
    };

    if (fbuser) {
        $scope.facebook = true
    }

    var user = firebase.auth().currentUser;
    if (user) {
        $state.go('tab.home')
    } else if (fbuser) {
        $state.go('tab.home')
    } else {

    }

    // IF FB WORKS!!!
    var fbLoginSuccess = function(response) {
        $scope.fblogged = true;

        if (!response.authResponse) {
            fbLoginError("Cannot find the authResponse");
            return;
        }

        var authResponse = response.authResponse;

        getFacebookProfileInfo(authResponse)
            .then(function(profileInfo) {

                store.set('fbid', profileInfo.id)
                store.set('fbname', profileInfo.name)

                $scope.usersusername = profileInfo.id;

                var ref = firebase.database().ref();
                var codesRef = ref.child("Codes");
                var usersRef = codesRef.child($scope.usersusername);
                var infoRef = usersRef.child('User Information');

                infoRef.set({
                    username: profileInfo.name,
                    provider: 'facebook',
                    SystemMessage: false,
                    PlayerPoints: 0,
                    id: profileInfo.id,
                    battlingstate: false
                        // picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
                });



                // store.setUser({
                //     authResponse: success.authResponse,
                //     userID: profileInfo.id,
                //     name: profileInfo.name,
                //     email: profileInfo.email,
                //     picture: "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
                // });

                $state.go('tab.home');
            }, function(fail) {
                $ionicPopup.alert({
                    template: 'profile info fail' + fail,
                    cssClass: 'adminpop'
                });
            });
    };

    // This is the fail callback from the login method
    var fbLoginError = function(error) {
        $ionicPopup.alert({
            template: error,
            cssClass: 'adminpop'
        });

        $state.go('tab.login');

        $ionicLoading.hide();

    };

    // This method is to get the user profile info from the facebook api
    var getFacebookProfileInfo = function(authResponse) {
        var info = $q.defer();

        facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
            function(response) {
                console.log(response);
                info.resolve(response);
            },
            function(response) {
                console.log(response);
                info.reject(response);
            }
        );
        return info.promise;
    };

    $scope.facebooklogin = function() {
        facebookConnectPlugin.getLoginStatus(function(success) {
            if (success.status === 'connected') {
                console.log('getLoginStatus', success.status);
                var user = store.get('fbid');

                if (!user) {
                    getFacebookProfileInfo(success.authResponse)
                        .then(function(profileInfo) {

                            $scope.usersusername = profileInfo.id;

                            var ref = firebase.database().ref();
                            var codesRef = ref.child("Codes");
                            var usersRef = codesRef.child($scope.usersusername);
                            var infoRef = usersRef.child('User Information');

                            infoRef.set({
                                username: profileInfo.name,
                                provider: 'facebook',
                                SystemMessage: false,
                                PlayerPoints: 0,
                                id: profileInfo.id,
                                battlingstate: false
                                    // picture : "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
                            });

                            store.set('fbid', profileInfo.id)
                            store.set('fbname', profileInfo.name)

                            // store.setUser({
                            //     authResponse: success.authResponse,
                            //     userID: profileInfo.id,
                            //     name: profileInfo.name,
                            //     email: profileInfo.email,
                            //     picture: "http://graph.facebook.com/" + success.authResponse.userID + "/picture?type=large"
                            // });


                            $state.go('tab.home');


                        }, function(fail) {
                            $ionicPopup.alert({
                                template: 'profile info fail' + fail,
                                cssClass: 'adminpop'
                            });
                        });
                } else {
                    $state.go('tab.home');
                }
            } else {

                console.log('getLoginStatus', success.status);

                $ionicLoading.show({
                    template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>',
                    duration: 2000
                })

                $state.go('tab.home');
                facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
            }
        });

    };

    $scope.authObj = $firebaseAuth();

    $scope.customsign = function(email, password) {

        $ionicLoading.show({
            template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>',
            duration: 2000
        })

        $scope.authObj.$createUserWithEmailAndPassword(email, password).then(function(firebaseUser) {
            $scope.customlogin(email, password)
        }).catch(function(error) {
            var alertPopup = $ionicPopup.alert({
                template: error,
                cssClass: 'adminpop'
            });

            console.log("Error creating user:", error);
        });


    };

    $scope.user = store.get('fbid');

    $scope.logout = function() {

        var hideSheet = $ionicActionSheet.show({
            destructiveText: 'Logout',
            titleText: 'Are you sure you want to logout of Venator?',
            cancelText: 'Cancel',
            cancel: function() {},
            buttonClicked: function(index) {
                return true;
            },
            destructiveButtonClicked: function() {
                $ionicLoading.show({
                    template: 'Logging out...'
                });


                store.set('playermode', 1);

                store.set('enemyhp', 0);
                store.set('hp', 0);

                store.set('eatk', 0);
                store.set('edef', 0);
                store.set('estn', 0);
                store.set('myatk', 0);
                store.set('mydef', 0);
                store.set('mystn', 0);

                store.set('creatureName', 0);
                store.set('creatureImage', 0);
                store.set('creatureCustomCss', 0);
                store.set('creatureShiny', 0);
                store.set('creatureLvl', 0);
                store.set('creatureRarity', 0);
                store.set('creatureCode', 0);

                store.set('isinBattle', false);
                store.set('isinBattlelvl', 0);
                store.set('isinBattleEsc', false);

                // Facebook logout
                facebookConnectPlugin.logout(function() {
                        $ionicLoading.hide();

                        $state.go('login');
                    },
                    function(fail) {
                        $ionicLoading.hide();
                    });
            }
        });
    };

    $scope.customlogin = function(email, password) {

        $ionicLoading.show({
            template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>',
            duration: 2000
        })

        $scope.authObj.$signInWithEmailAndPassword(email, password).then(function(firebaseUser) {

            $scope.usersusername = firebaseUser.uid;

            var ref = firebase.database().ref();
            var codesRef = ref.child("Codes");
            var usersRef = codesRef.child($scope.usersusername);
            var infoRef = usersRef.child('User Information');

            infoRef.set({
                email: email,
                SystemMessage: 'Welcome to Venator. If you are having trouble scanning try turning your flashlight on!',
                PlayerPoints: 0,
                id: firebaseUser.uid,
                battlingstate: false
            });

            store.set('pwid', firebaseUser.uid)
            store.set('pname', email)

            $state.go('tab.home');

        }).catch(function(error) {
            $ionicLoading.hide();

            var alertPopup = $ionicPopup.alert({
                template: error,
                cssClass: 'adminpop'
            });
        });

    };

})

.controller('MonstersCtrl', function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicModal, $state, $firebaseArray, $ionicPopup, $ionicLoading, store, $firebaseObject, $timeout, $ionicScrollDelegate, $location, $ionicPopover) {

    $scope.eggallow = false;

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    var fname = store.get('fbname');
    var pwname = store.get('pname');

    if (fbuser) {
        $scope.username = fbuser;
        $scope.displayname = fname;
    } else {
        $scope.username = puser;
        $scope.displayname = pwname;
    }

    var myfriendsref = firebase.database().ref().child("Codes");
    var monsterpull = myfriendsref.child($scope.username);

    $scope.usersRefs = $firebaseArray(monsterpull);

    var monsterpull = myfriendsref.child($scope.username);

    var traitspull = monsterpull.child('Traits');
    $scope.traits = $firebaseArray(traitspull);

    $scope.usersRefs.$watch(function() {
        $scope.usersRefs = $firebaseArray(monsterpull);
    });

    $scope.usersRefs.$loaded(function() {
        var dataExists = $scope.usersRefs.$value !== null;
    });

    var connectedRef = firebase.database().ref().child(".info/connected");
    connectedRef.on("value", function(snap) {
        if (snap.val() === true) {
            $ionicLoading.hide().then(function() {
                $scope.offline = false
            });
        } else {
            $ionicLoading.hide().then(function() {
                $scope.offline = true
            });
        }
    });


    $scope.enablebeast = function() {
        $scope.eggallow = false;
        $scope.techsection = false;
        $scope.descsection = false;
        $scope.amonsterselected = false;
        $scope.monstersecselected = false;

        $timeout(function() {
            $location.hash('top');
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });

    }

    $scope.enableegg = function() {
        $scope.eggallow = true;
        $scope.techsection = false;
        $scope.descsection = false;
        $scope.amonsterselected = false;
        $scope.monstersecselected = true;

        $scope.visicheck = 'all';

        $timeout(function() {
            $location.hash('top');
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });
    }

    $scope.data = {}
    $scope.activeMenu = $scope.usersRefs[0];

    $scope.makereport = function(Monstername) {
        var alertPopup = $ionicPopup.alert({
            template: '<input type="text" placeholder="Report info..." ng-model="data.pass" style="text-align: left;"></input> <div ng-if="fries == false" >Please enter something</div>',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Exit'
            }, {
                text: 'Go',
                type: 'button-positive',
                onTap: function(e) {
                    if ($scope.data.pass) {
                        var rootRef = firebase.database().ref().child('Reports');

                        rootRef.once("value", function(snapshot) {
                            $scope.thecount = snapshot.numChildren();

                            rootRef.child($scope.thecount + 1).set({
                                "report": $scope.data.pass,
                                "monster": Monstername,
                                "user": $scope.displayname,
                                "time": firebase.database.ServerValue.TIMESTAMP
                            });

                        });

                    } else {
                        e.preventDefault();
                        $scope.fries = false
                    }
                }
            }]
        });
    }

    $scope.enabletech = function() {

        $scope.techsection = true;
        $scope.descsection = false;
        $scope.amonsterselected = false;
        $scope.monstersecselected = true;

        $scope.visicheck = 'all';

        $timeout(function() {
            $location.hash('top');
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });

    }

    $scope.enabledesc = function() {

        $scope.descsection = true;
        $scope.techsection = false;
        $scope.amonsterselected = false;
        $scope.monstersecselected = true;

        $scope.visicheck = 'all';

        $timeout(function() {
            $location.hash('top');
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });

    }



    $scope.barcodepage = function() {
        $state.go('tab.barcode')
    }


    $scope.addFav = function(usersRef) {

        if (usersRef.favourite) {
            $scope.activeMenu = usersRef

            $timeout(function() {
                $scope.activeMenu = null
            }, 3000);
        } else {
            var rootRef = firebase.database().ref().child('Codes');
            var membersRef = rootRef.child($scope.username);
            var thirdRef = membersRef.child(usersRef.barcode);

            var corearray3 = $firebaseArray(membersRef);

            corearray3.$loaded(function(x) {
                $scope.secondresults = corearray3;

                var firstresult = $scope.secondresults;

                $scope.myfav2count = 0;

                angular.forEach(firstresult, function(value, key) {
                    if (value.favourite == true) {
                        $scope.myfav2count += 1;
                    } else {}
                });

                if ($scope.myfav2count < 6) {
                    var objectToSave = {};
                    var user_name = 'favourite';
                    objectToSave = true;

                    thirdRef.child(user_name).set(objectToSave);
                    console.log('Added to favourites')

                    $timeout(function() {
                        $location.hash(usersRef.barcode);
                        var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
                        handle.anchorScroll();
                    });

                } else {
                    var alertPopup = $ionicPopup.alert({
                        template: 'You\'re deck is full. Please remove a character by holding it\'s card in your deck.',
                        cssClass: 'adminpop'
                    });
                }

            });

        }

    }

    var checkeggcountref = firebase.database().ref().child('Codes').child($scope.username);
    var checkeggcount = $firebaseArray(checkeggcountref);
    $scope.nomonsters = false

    if (checkeggcount.$loaded()) {
        checkeggcount.$watch(function() {
            $scope.eggscount = 0;
            $scope.monstercount = 0;
            $scope.favcount = 0;
            $scope.techcount = 0;
            $scope.desccount = 0;

            $scope.eggcount = checkeggcount;
            var fineggcount = $scope.eggcount;
            var itemsProcessed = 0;

            angular.forEach(fineggcount, function(value, key) {
                itemsProcessed++;

                if (value.Monster && value.Monsterlvl >= 1) {
                    $scope.monstercount = $scope.monstercount + 1
                }

                if (value.favourite && value.Monster) {
                    $scope.favcount = $scope.favcount + 1
                }

                if (value.Catagory == "Technology" && value.Monster) {
                    $scope.techcount = $scope.techcount + 1
                }

                if (value.Catagory == "Discovery" && value.Monster) {
                    $scope.desccount = $scope.desccount + 1
                }

                if (value.Monsterlvl == 0 || !value.Monsterlvl && !value.Type) {
                    if (value.Monster) {
                        $scope.eggscount = $scope.eggscount + 1
                    }
                }

                $scope.totalmonsterscount = $scope.monstercount + $scope.techcount + $scope.desccount
            });

            $timeout(function() {
                console.log("Monsters:" + $scope.monstercount + " Tech:" + $scope.techcount + " Descoveries:" + $scope.desccount + " Eggs:" + $scope.eggscount + " Favourites:" + $scope.favcount)
                store.set('amountOfMonsters', $scope.monstercount)
            }, 1000);
        });
    }

    $scope.hideifnonea = function() {
        $scope.hideifnone = true;
    }

    $scope.hideifnoneb = function() {
        $scope.hideifnonec = true;
    }

    $scope.menuClass = function(usersRef) {
        console.log(usersRef)
        var current = $location.path().substring(1);
        return usersRef === current ? "usersRef" : "";
    };

    $scope.remFav = function(usersRef) {

        $timeout(function() {
            $location.hash(usersRef.barcode);
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });

        var rootRef = firebase.database().ref().child('Codes');
        var membersRef = rootRef.child($scope.username);
        var thirdRef = membersRef.child(usersRef.barcode);

        var objectToSave = {};
        var user_name = 'favourite';
        objectToSave = false;

        thirdRef.child(user_name).set(objectToSave);
        console.log('Removed from favourites');

    }

    $scope.cantFav = function(usersRef) {
        var alertPopup = $ionicPopup.alert({
            template: 'You must hatch this egg in the dojo before adding to your deck.',
            cssClass: 'adminpop'
        });
    }

    $scope.remove = function(Monstercode) {
        console.log($scope.Monstercode)

        var alertPopup = $ionicPopup.alert({
            // for <b>' + $scope.Monsterworth + '</b> Halolecter
            template: 'Are you sure that you would like to set this creature free?',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Cancel'
            }, {
                text: 'Confirm',
                type: 'button-positive',
                onTap: function(e) {
                    var rootRef = firebase.database().ref().child('Codes/');
                    var membersRef = rootRef.child($scope.username);
                    var monsterRef = membersRef.child($scope.Monstercode)
                    var todeleteobj = $firebaseObject(monsterRef);

                    alertPopup.then(function(res) {
                        $scope.amonsterselected = false;

                        todeleteobj.$remove().then(function(ref) {
                            console.log("CREATURE REMOVED")
                        }, function(error) {
                            console.log("Error:", error);
                        });
                    });

                }
            }]
        });
    }

    $ionicModal.fromTemplateUrl('raritysearchmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.raritysearchmodal = modal;
    });

    $scope.openraritysearchmodal = function(Monstername, Monsterrarity, Monsterimg, Monsterdesc, Monsterart, Monsterenviroment) {
        $scope.raritysearchmodal.show();

        $scope.Monstername = Monstername;
        $scope.MonsterRarity = Monsterrarity

        console.log($scope.MonsterRarity)

    };

    $scope.openrarspecmodal = function(usersRef) {

        $scope.Monstername = usersRef.Monster;
        $scope.Monsterimg = usersRef.MonsterImg;
        $scope.Monsterdesc = usersRef.MonsterDesc;
        $scope.Monsterrarity = usersRef.MonsterRarity;
        $scope.Monsterenvo = usersRef.MonsterEnviroment;
        $scope.Monsterart = usersRef.MonsterArt;

        $ionicPopup.alert({
            templateUrl: 'owned.html',
            cssClass: 'adminpop',
            scope: $scope
        });
    };


    $scope.closeraritysearchmodal = function() {
        $scope.raritysearchmodal.hide();
    };


    $ionicModal.fromTemplateUrl('amonster.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.modal3 = modal;
    });

    $scope.openModal2 = function(usersRef) {
        $scope.egg = false;
        usersRef.status = !usersRef.status;

        if (usersRef.status) {
            $scope.amonsterselected = true
        } else {
            $scope.amonsterselected = false
        }

        $scope.Monstername = usersRef.Monster;
        $scope.Monsterimg = usersRef.MonsterImg;
        $scope.Monsterdesc = usersRef.MonsterDesc;
        $scope.Monstercode = usersRef.barcode;
        $scope.Monsterrarity = usersRef.MonsterRarity;
        $scope.Monsterenvo = usersRef.MonsterEnviroment;
        $scope.Monsterart = usersRef.MonsterArt;
        $scope.Monsterstatus = usersRef.MonsterStatus;
        $scope.CustomCss = usersRef.CustomCss;
        $scope.RemCount = usersRef.RemCollector;
        $scope.Count = usersRef.Collector;
        $scope.Monstertype = usersRef.Type;


        $scope.Monsterlvl = usersRef.Monsterlvl;

    };

    $scope.cansearch = false
    $scope.levelup = false;

    $scope.openModal3 = function(usersRef) {
        $scope.amonsterselected = true
        $scope.levelup = false;


        $scope.Monstername = usersRef.Monster;
        $scope.Monsterimg = usersRef.MonsterImg;
        $scope.Monsterdesc = usersRef.MonsterDesc;
        $scope.Monstercode = usersRef.barcode;
        $scope.Monsterrarity = usersRef.MonsterRarity;
        $scope.Monsterenvo = usersRef.MonsterEnviroment;
        $scope.Monsterart = usersRef.MonsterArt;
        $scope.Monsterstatus = usersRef.MonsterStatus;
        $scope.CustomCss = usersRef.CustomCss;
        $scope.MonsterID = usersRef.id;
        $scope.Monstertype = usersRef.Type;
        $scope.Catagory = usersRef.Catagory;

        $scope.Shiny = usersRef.shiny;

        $scope.RemCount = usersRef.RemCollector;
        $scope.Count = usersRef.Collector;

        $scope.customcss = usersRef.CustomCss;

        $scope.namelength = $scope.Monstername.length

        if (usersRef.Monsterlvl) {
            $scope.Monsterlvl = usersRef.Monsterlvl;
            $scope.egg = false;
        } else {
            $scope.Monsterlvl = 0;
            $scope.egg = true;
        }

        var mtraiteref = firebase.database().ref().child("Codes").child($scope.username).child($scope.Monstercode).child("Traites")
        $scope.myTraites = $firebaseArray(mtraiteref)
    };

    $scope.assignTraite = function(Monstercode, Monsterenvo) {

        $scope.theEnv = Monsterenvo;

        console.log($scope.theEnv);

        var traiteref = firebase.database().ref().child("Traites");
        $scope.traites = $firebaseArray(traiteref)
        console.log($scope.traites)

        $ionicPopup.alert({
            templateUrl: 'traitpopse.html',
            cssClass: 'traitpop',
            buttons: [{
                text: 'Confirm',
                type: 'button-positive',
                onTap: function(e) {
                    if ($scope.traitcount == 5) {

                        selectedtraits.length == 5

                        angular.forEach(selectedtraits, function(value, key) {
                            var mtraiteref = firebase.database().ref().child("Codes").child($scope.username).child($scope.Monstercode).child("Traites").child(key)
                            mtraiteref.set({
                                Name: value.Name,
                                Enviroment: value.Enviroment,
                                Desc: value.Desc,
                                Attack: value.ATK,
                                Defense: value.DEF,
                                Stun: value.STN
                            });

                            console.log("Saved Traite " + value.Name)
                        });


                        $scope.traitcount = 0;

                    } else {
                        e.preventDefault();
                        $scope.requiresfive = true;

                    }
                }
            }, {
                text: 'Cancel',
                type: 'button-negative',
                onTap: function(e) {
                    $scope.traitcount = 0;
                }
            }],
            scope: $scope
        });
    }

    $scope.traitcount = 0;

    var selectedtraits = [];

    $scope.selectcard = function(key) {

        if (!$scope.traites[key]) {
            key = 1;
        }

        if ($scope.traites[key]) {
            if ($scope.traites[key].selected) {
                $scope.alerttai = false;
                $scope.traites[key].selected = false;
                $scope.traitcount = $scope.traitcount - 1

                var index = selectedtraits.indexOf(key);
                selectedtraits.splice(index, 1);

            } else {
                if ($scope.traitcount <= 4) {
                    $scope.alerttai = false;
                    $scope.traites[key].selected = true;
                    $scope.traitcount = $scope.traitcount + 1

                    selectedtraits.push({
                        Name: $scope.traites[key].Name,
                        Desc: $scope.traites[key].Desc,
                        ATK: $scope.traites[key].Attack,
                        DEF: $scope.traites[key].Defense,
                        STN: $scope.traites[key].Stun,
                        Enviroment: $scope.traites[key].Enviroment,
                    });

                } else {
                    $scope.alerttai = true
                }
            }
        } else {
            $ionicPopup.alert({
                template: 'Error selecting card, please try again',
                cssClass: 'adminpop',
            });
        }

        $scope.traitsArray = selectedtraits;
        console.log($scope.traitsArray)

    }

    $scope.levelmonup = function(Monsterlvl, Monstername, Monsterimg, Monstercode) {
        $scope.levelup = true

        $scope.Monstername = Monstername;
        $scope.Monsterimg = Monsterimg;
        $scope.Monsterlvl = Monsterlvl;
        $scope.Monstercode = Monstercode;

        $timeout(function() {
            $scope.delayer = true
        }, 800);

        if (Monsterlvl == 0) {
            $scope.egg = true;
        } else {
            $scope.egg = false;
        }

        var getclickcount = store.get(Monstercode)

        if (getclickcount) {
            $scope.tapcount = getclickcount
        } else {
            store.set(Monstercode, 50)
            $scope.tapcount = 50
        }

    };

    $scope.untap = function() {

        $timeout(function() {
            $scope.levelup = false;
            $scope.delayer = false;
        });

    };

    $scope.visicheck = 'none';

    $scope.dale = function() {

        $timeout(function() {
            $scope.isdale = true;
            $scope.visicheck = 'all';
        }, 1000);
    };

    $scope.dale2 = function() {

        $timeout(function() {
            $scope.isdale2 = true;
            $scope.visicheck = 'all';
        }, 1000);

    };

    $scope.tap = function(Monsterlvl, Monstername, Monstercode, Monsterimg, CustomCss) {
        var tapref = firebase.database().ref().child("Codes/").child($scope.username).child(Monstercode);
        var taprefd = tapref.child('Monsterlvl');

        var levelobject = $firebaseObject(taprefd);
        var getclickcount = store.get(Monstercode)

        levelobject.$loaded().then(function() {
            if (getclickcount == null) {
                store.set(Monstercode, 50)
                $scope.Monsterlvl = 0
            } else if (getclickcount == 0) {
                store.set(Monstercode, $scope.Monsterlvl * 50)

                $scope.tapcount = getclickcount

                $scope.Monsterlvl = levelobject.$value;

                if (!levelobject.$value) {
                    levelobject.$value = 1;
                    levelobject.$save();
                    $scope.egg = false;

                    $scope.Monsterlvl = levelobject.$value;
                    $scope.Monstername = Monstername;
                    $scope.Monsterimg = Monsterimg;
                    $scope.CustomCss = CustomCss

                    $ionicPopup.alert({
                        templateUrl: 'owned.html',
                        cssClass: 'adminpop animated tada',
                        scope: $scope
                    });

                } else {
                    console.log($scope.Monsterlvl)
                    levelobject.$value = $scope.Monsterlvl + 1;
                    levelobject.$save();

                    $scope.Monsterlvl = levelobject.$value;
                    $scope.Monstername = Monstername;

                    $ionicPopup.alert({
                        template: 'Your ' + $scope.Monstername + ' has reached lvl.' + $scope.Monsterlvl,
                        cssClass: 'adminpop',
                        scope: $scope
                    });

                }

            } else {

                if ($scope.Monsterlvl) {
                    store.set(Monstercode, getclickcount - 1)
                    console.log(getclickcount)

                    $scope.tapcount = getclickcount
                } else {
                    $scope.mynum = 50
                    store.set(Monstercode, $scope.mynum - 1)
                    $scope.Monsterlvl = 1;

                    $scope.tapcount = getclickcount
                }

            }
        });


    };

    $scope.closeModal2 = function(Monstercode) {
        $scope.amonsterselected = false;

        $timeout(function() {
            $location.hash(Monstercode);
            var handle = $ionicScrollDelegate.$getByHandle('scrollcon');
            handle.anchorScroll();
        });

    };

    $scope.bighead = false;

    $scope.imagetap = function() {
        if ($scope.bighead) {
            $scope.bighead = false;
        } else {
            $scope.bighead = true;
        }
    };

    $ionicModal.fromTemplateUrl('monsterdex.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal2 = modal;
    });

    $scope.openModal = function() {
        $scope.modal2.show();
    };

    $scope.closeModal = function() {
        $scope.modal2.hide();
    };

    $scope.init = function() {
        $ionicSlideBoxDelegate.update();
    };

    $scope.$on('$stateChangeSuccess', function() {
        $ionicSlideBoxDelegate.update();
    });


    $scope.doRefresh = function() {
        // $scope.monsters = Monsters;
        // $scope.codes = Codes;
        $timeout(function() {
            if (checkeggcount.$loaded()) {
                checkeggcount.$watch(function() {
                    $scope.eggscount = 0;
                    $scope.monstercount = 0;
                    $scope.favcount = 0;
                    $scope.techcount = 0;
                    $scope.desccount = 0;

                    $scope.eggcount = checkeggcount;
                    var fineggcount = $scope.eggcount;

                    angular.forEach(fineggcount, function(value, key) {

                        if (value.Monster && value.Monsterlvl >= 1) {
                            $scope.monstercount = $scope.monstercount + 1
                        }

                        if (value.Monster && !value.MonsterEnviroment) {
                            $scope.monstercount = $scope.monstercount + 1
                        }

                        if (value.favourite && value.Monster) {
                            $scope.favcount = $scope.favcount + 1
                        }

                        if (value.Catagory == "Technology" && value.Monster) {
                            $scope.techcount = $scope.techcount + 1
                        }

                        if (value.Catagory == "Discovery" && value.Monster) {
                            $scope.desccount = $scope.desccount + 1
                        }

                        if (value.Monsterlvl == 0 || !value.Monsterlvl && !value.Type) {
                            if (value.Monster) {
                                $scope.eggscount = $scope.eggscount + 1
                            }
                        }

                    });

                    console.log("Monsters:" + $scope.monstercount + " Tech:" + $scope.techcount + " Descoveries:" + $scope.desccount + " Eggs:" + $scope.eggscount + " Favourites:" + $scope.favcount)
                });

                $scope.$broadcast('scroll.refreshComplete');
            }
        }, 1500);
    };

    $ionicPopover.fromTemplateUrl('my-popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.propertyName = '-time';

    $scope.findorder = function(propertyName) {
        $scope.propertyName = propertyName;
        $scope.popover.hide();
    };

    $scope.sortby = function($event, Monster) {
        $scope.popover.show($event);
        console.log(Monster)
    };

    $scope.showtraite = function(myTraite) {
        var lowerConverter = myTraite.Enviroment.toLowerCase();
        
        $ionicPopup.alert({
            template: "<img class='popupimageplanet' ng-src='img/planets/" + lowerConverter + ".png' src='img/skull.gif'><span class='liltext'> " + myTraite.Enviroment + " Traite </span> <h3> " + myTraite.Name + " </h3> <div class='traitstats'><div class='row'><div class='col'>ATK " + myTraite.Attack + "</div><div class='col'>DEF" + myTraite.Defense + "</div><div class='col'>STN " + myTraite.Stun + "</div></div></div><h4> " + myTraite.Desc + "</h4>",
            cssClass: 'adminpop'
        });
    };
})


.controller('DreamerCtrl', function($scope, $stateParams, $ionicModal, $state, Monsters, $ionicPopover, $ionicPopup, $http, $ionicLoading, store, Technology, $firebaseArray) {
    $scope.cancollect = false;
    $scope.data = {};

    $ionicModal.fromTemplateUrl('dreamerpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $ionicModal.fromTemplateUrl('techpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.techmodal = modal;
    });

    $ionicModal.fromTemplateUrl('descoverpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.descmodal = modal;
    });

    $ionicModal.fromTemplateUrl('editpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.editmodal = modal;
    });

    $ionicModal.fromTemplateUrl('edittechpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.edittechmodal = modal;
    });

    $ionicModal.fromTemplateUrl('desceditpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.editdescmodal = modal;
    });

    var connectedRef = firebase.database().ref().child(".info/connected");
    connectedRef.on("value", function(snap) {
        if (snap.val() === true) {
            $ionicLoading.hide().then(function() {
                $scope.offline = false
            });
        } else {
            $ionicLoading.hide().then(function() {
                $scope.offline = true
            });
        }
    });

    $scope.submit = function(form, formData) {
        if (form.$valid) { //submit form if it is valid
            $http.post('/url', {
                    data: formData
                })
                .success(function() {
                    //code here
                })
                .error(function() {})
        } else {
            var alertPopup = $ionicPopup.alert({
                template: 'There was an error with your form, please check you have filled everything out correctly, and try again.',
                cssClass: 'adminpop'
            });
        }
    }

    $scope.showeditmodal = function(monster) {
        $scope.editmodal.show();

        $scope.monstername = monster.name
        $scope.monsters.name = monster.name
        $scope.monsters.desc = monster.desc
        $scope.monsters.enviroment = monster.enviroment
        $scope.monsters.imgurl = monster.imgurl
        $scope.monsters.CustomCss = monster.CustomCss
        $scope.monsters.artist = monster.artist
        $scope.monsters.rarity = monster.rarity
        $scope.monsters.collectorcount = monster.collectorcount
        $scope.monsters.monetae = monster.monetae
        $scope.monsterid = monster.id
    }

    $scope.showedittechmodal = function(monster) {
        $scope.edittechmodal.show();

        $scope.monstername = monster.name
        $scope.monsters.name = monster.name
        $scope.monsters.desc = monster.desc
        $scope.monsters.type = monster.type
        $scope.monsters.imgurl = monster.imgurl
        $scope.monsters.CustomCss = monster.CustomCss
        $scope.monsters.artist = monster.artist
        $scope.monsters.rarity = monster.rarity
        $scope.monsters.collectorcount = monster.collectorcount
        $scope.monsters.monetae = monster.monetae
        $scope.monsterid = monster.id
    }

    $scope.showeditdescmodal = function(monster) {
        $scope.editdescmodal.show();

        $scope.monstername = monster.name
        $scope.monsters.name = monster.name
        $scope.monsters.desc = monster.desc
        $scope.monsters.type = monster.type
        $scope.monsters.imgurl = monster.imgurl
        $scope.monsters.CustomCss = monster.CustomCss
        $scope.monsters.artist = monster.artist
        $scope.monsters.rarity = monster.rarity
        $scope.monsters.collectorcount = monster.collectorcount
        $scope.monsters.monetae = monster.monetae
        $scope.monsterid = monster.id
    }


    $scope.openModal = function() {

        var alertPopup = $ionicPopup.alert({
            template: 'Ask your parents prior to submitting anything to Venator.',
            scope: $scope,
            cssClass: 'adminpop popup-vertical-buttons',
            buttons: [{
                text: 'Add creature',
                type: 'button-positive',
                onTap: function(e) {
                    $scope.modal.show();
                }
            }, {
                text: 'Add Technology',
                type: 'button-positive',
                onTap: function(e) {
                    $scope.techmodal.show();
                }
            }, {
                text: 'Add Discovery',
                type: 'button-positive',
                onTap: function(e) {
                    $scope.descmodal.show();
                }
            }, {
                text: 'Cancel',
                type: 'button-negative'
            }]
        });
    };


    $scope.closeModal = function() {
        $scope.modal.hide();
    };

    $scope.closeTechModal = function() {
        $scope.techmodal.hide();
    };


    $scope.closeEditTechModal = function() {
        $scope.edittechmodal.hide();
    };

    $scope.closeDescModal = function() {
        $scope.descmodal.hide();
    };

    $scope.clearModal = function() {
        $ionicPopup.alert({
            template: 'Are you sure that you would like to clear the form and start again?',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Cancel'
            }, {
                text: 'Confirm',
                type: 'button-positive',
                onTap: function(e) {
                    $scope.monstername = undefined
                    $scope.monsters.name = undefined
                    $scope.monsters.desc = undefined
                    $scope.monsters.type = undefined
                    $scope.monsters.imgurl = undefined
                    $scope.monsters.CustomCss = undefined
                    $scope.monsters.artist = undefined
                    $scope.monsters.rarity = undefined
                    $scope.monsters.collectorcount = undefined
                    $scope.monsters.monetae = undefined
                    $scope.monsterid = undefined
                    $scope.enviroment = undefined
                }
            }]
        });
    }


    $scope.closeEditDescModal = function() {
        $scope.editdescmodal.hide();
    };

    $scope.fromimageurl = function() {
        $scope.isfromimageurl = true
    };

    $scope.nahfromimageurl = function() {
        $scope.isfromimageurl = false
    };

    $scope.collect = function() {
        $ionicPopup.alert({
            template: '<input type="password" placeholder="Enter Collectors Password" ng-model="data.password"></input> <div ng-if="fries == false" >Incorrect Password, try again</div>',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Exit'
            }, {
                text: 'Go',
                type: 'button-positive',
                onTap: function(e) {
                    if ($scope.data.password == 'gc2017') {
                        $scope.cancollect = true;
                    } else {
                        e.preventDefault();
                        $scope.cancollect = false;
                        $scope.fries = false;
                    }
                }
            }]
        });

    };


    $scope.monsters = Monsters;
    $scope.technology = Technology;


    $scope.extinct = function(monster) {

        $scope.Monsterid = monster.id

        console.log($scope.Monsterid)

        var alertPopup = $ionicPopup.alert({
            template: 'Are you sure that you would like to remove this creature from Venator?',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Cancel'
            }, {
                text: 'Confirm',
                type: 'button-positive',
                onTap: function(e) {
                    var rootRef = firebase.database().ref().child('Monsters/');
                    var monsterRef = rootRef.child($scope.Monsterid)

                    var objectToSave = {};
                    var user_name = 'approved';
                    objectToSave = false;

                    monsterRef.child(user_name).set(objectToSave);
                    console.log('Added from favourites')

                    var alertPopup = $ionicPopup.alert({
                        template: 'Creature is no longer catchable',
                        cssClass: 'adminpop',
                    });

                }
            }]
        });
    }

    $scope.achange = function() {
        $scope.selectedfile = event.target.files[0];
        $scope.uploadfile();
    }

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    var fname = store.get('fbname');
    var pwname = store.get('pname');

    if (fbuser) {
        $scope.username = fbuser;
        $scope.displayname = fname;
    } else {
        $scope.username = puser;
        $scope.displayname = pwname;
    }

    $scope.uploadfile = function() {

        $scope.rand = Math.floor((Math.random() * 50000) + 1)
        console.log($scope.username + ", " + $scope.rand)

        var storageRef = firebase.storage().ref().child($scope.username).child('/' + $scope.rand);
        var file = $scope.selectedfile;

        // Create the file metadata
        var metadata = {
            contentType: 'image/jpeg'
        };

        // Upload file and metadata to the object 'images/mountains.jpg'
        var uploadTask = storageRef.child(file.name).put(file, metadata);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
            function(snapshot) {
                // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
                $scope.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                console.log('Upload is ' + $scope.progress + '% done');

                if ($scope.progress < 100) {
                    $scope.status = "Image Uploading"

                    $ionicLoading.show({
                        template: 'Uploading.. <br /><br /><ion-spinner class="spinner" icon="lines"></ion-spinner>'
                    })

                } else {
                    $scope.status = "Image Done"

                    $ionicLoading.hide()

                    var alertPopup = $ionicPopup.alert({
                        template: 'Upload Complete.',
                        cssClass: 'adminpop',
                    });

                }

                console.log($scope.status)
                switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'
                        console.log('Upload is paused');

                        $scope.loadingimg = false;

                        var alertPopup = $ionicPopup.alert({
                            template: 'Upload Paused.',
                            cssClass: 'adminpop',
                        });

                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'
                        console.log('Upload is running');
                        break;
                }
            },
            function(error) {
                $ionicLoading.hide()

                var alertPopup = $ionicPopup.alert({
                    template: error,
                    cssClass: 'adminpop',
                });
            },
            function() {
                // Upload completed successfully, now we can get the download URL
                var downloadURL = uploadTask.snapshot.downloadURL;
                console.log(downloadURL)

                $scope.me = downloadURL;

                $scope.loadingimg = false;
            });
    }

    $scope.submitbutton = function() {

        var foreachvar = $scope.traitsArray;

        if ($scope.traitsArray && $scope.traitsArray.length >= 5) {

            var ref = firebase.database().ref();
            var monstersRef = ref.child("Monsters");

            monstersRef.once("value", function(snapshot) {
                $scope.monsters.count = snapshot.numChildren();
            });

            if ($scope.monsters.collectorcount) {
                $scope.collectorcount = $scope.monsters.collectorcount
            } else {
                $scope.collectorcount = false
            }

            if ($scope.me) {
                monstersRef.child($scope.monsters.count + 1).set({
                    "name": $scope.monsters.name,
                    "desc": $scope.monsters.desc,
                    "artist": $scope.monsters.artist,
                    "imgurl": $scope.me,
                    "rarity": $scope.monsters.rarity,
                    "monetae": $scope.monsters.monetae,
                    "enviroment": $scope.monsters.enviroment,
                    'time': firebase.database.ServerValue.TIMESTAMP,
                    // "imgtype": $scope.monsters.imgtype,
                    "approved": 'pend',
                    id: $scope.monsters.count + 1,
                    "CustomCss": $scope.monsters.CustomCss,
                    "collectorcount": $scope.collectorcount,
                    "remcollectorcount": $scope.collectorcount,
                    "user": $scope.displayname,
                    "catagory": 'Creature',
                });
            } else {
                monstersRef.child($scope.monsters.count + 1).set({
                    "name": $scope.monsters.name,
                    "desc": $scope.monsters.desc,
                    "artist": $scope.monsters.artist,
                    "imgurl": $scope.monsters.imgurl,
                    "rarity": $scope.monsters.rarity,
                    "monetae": $scope.monsters.monetae,
                    "enviroment": $scope.monsters.enviroment,
                    'time': firebase.database.ServerValue.TIMESTAMP,
                    // "imgtype": $scope.monsters.imgtype,
                    "approved": 'pend',
                    id: $scope.monsters.count + 1,
                    "CustomCss": $scope.monsters.CustomCss,
                    "collectorcount": $scope.collectorcount,
                    "remcollectorcount": $scope.collectorcount,
                    "user": $scope.displayname,
                    "catagory": 'Creature'
                });
            }

            angular.forEach(foreachvar, function(value, key) {
                var traitRef = monstersRef.child($scope.monsters.count + 1).child('Traits').child(key).set({
                    "Name": value.Name,
                    "Desc": value.Desc,
                    "ATK": value.ATK,
                    "DEF": value.DEF,
                    "STN": value.STN,
                    "Enviroment": value.Enviroment,
                })
            });


            var alertPopup = $ionicPopup.alert({
                title: 'Thank You',
                template: 'Your monsters status can be viewed on your Dreams page.'
            });


            alertPopup.then(function(res) {
                $scope.modal.hide();
            });
        } else {
            $ionicPopup.alert({
                template: 'Please select five traits',
                cssClass: 'adminpop',
            });
        }

    };

    $scope.submitdesc = function() {

        var ref = firebase.database().ref();
        var monstersRef = ref.child("Monsters");

        monstersRef.once("value", function(snapshot) {
            $scope.monsters.count = snapshot.numChildren();
        });

        if ($scope.monsters.collectorcount) {
            $scope.collectorcount = $scope.monsters.collectorcount
        } else {
            $scope.collectorcount = false
        }

        if ($scope.me) {
            monstersRef.child($scope.monsters.count + 1).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.me,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: $scope.monsters.count + 1,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Discovery'
            });
        } else {
            monstersRef.child($scope.monsters.count + 1).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.monsters.imgurl,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: $scope.monsters.count + 1,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Discovery'
            });
        }



        var alertPopup = $ionicPopup.alert({
            title: 'Thank You',
            template: 'Your discovery status can be viewed on your Dreams page.'
        });


        alertPopup.then(function(res) {
            $scope.descmodal.hide();
        });
    };

    $scope.submittech = function() {

        var ref = firebase.database().ref();
        var monstersRef = ref.child("Monsters");

        monstersRef.once("value", function(snapshot) {
            $scope.monsters.count = snapshot.numChildren();
        });

        if ($scope.monsters.collectorcount) {
            $scope.collectorcount = $scope.monsters.collectorcount
        } else {
            $scope.collectorcount = false
        }

        if ($scope.me) {
            monstersRef.child($scope.monsters.count + 1).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.me,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: $scope.monsters.count + 1,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Technology'
            });
        } else {
            monstersRef.child($scope.monsters.count + 1).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.monsters.imgurl,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: $scope.monsters.count + 1,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Technology'
            });
        }



        var alertPopup = $ionicPopup.alert({
            title: 'Thank You',
            template: 'Your technology status can be viewed on your Dreams page.'
        });


        alertPopup.then(function(res) {
            $scope.techmodal.hide();
        });
    };

    $scope.submitedit = function(monsterid) {

        var foreachvar = $scope.traitsArray;

        if ($scope.traitsArray && $scope.traitsArray.length >= 5) {

            var ref = firebase.database().ref();
            var monstersRef = ref.child("Monsters");

            if ($scope.monsters.collectorcount) {
                $scope.collectorcount = $scope.monsters.collectorcount
            } else {
                $scope.collectorcount = false
            }

            if ($scope.me) {
                monstersRef.child(monsterid).set({
                    "name": $scope.monsters.name,
                    "desc": $scope.monsters.desc,
                    "artist": $scope.monsters.artist,
                    "imgurl": $scope.me,
                    "rarity": $scope.monsters.rarity,
                    "monetae": $scope.monsters.monetae,
                    "enviroment": $scope.monsters.enviroment,
                    'time': firebase.database.ServerValue.TIMESTAMP,
                    // "imgtype": $scope.monsters.imgtype,
                    "approved": 'pend',
                    id: monsterid,
                    "CustomCss": $scope.monsters.CustomCss,
                    "collectorcount": $scope.collectorcount,
                    "remcollectorcount": $scope.collectorcount,
                    "user": $scope.displayname,
                    "catagory": 'Creature'
                });
            } else {
                monstersRef.child(monsterid).set({
                    "name": $scope.monsters.name,
                    "desc": $scope.monsters.desc,
                    "artist": $scope.monsters.artist,
                    "imgurl": $scope.monsters.imgurl,
                    "rarity": $scope.monsters.rarity,
                    "monetae": $scope.monsters.monetae,
                    "enviroment": $scope.monsters.enviroment,
                    'time': firebase.database.ServerValue.TIMESTAMP,
                    // "imgtype": $scope.monsters.imgtype,
                    "approved": 'pend',
                    id: monsterid,
                    "CustomCss": $scope.monsters.CustomCss,
                    "collectorcount": $scope.collectorcount,
                    "remcollectorcount": $scope.collectorcount,
                    "user": $scope.displayname,
                    "catagory": 'Creature'
                });
            }

            angular.forEach(foreachvar, function(value, key) {
                var traitRef = monstersRef.child(monsterid).child('Traits').child(key).set({
                    "Name": value.Name,
                    "Desc": value.Desc,
                    "ATK": value.ATK,
                    "DEF": value.DEF,
                    "STN": value.STN,
                    "Enviroment": value.Enviroment,
                })
            });


            var alertPopup = $ionicPopup.alert({
                title: 'Changes Saved',
                template: 'Your monsters status can be viewed on your Dreams page.'
            });


            alertPopup.then(function(res) {
                $scope.modal.hide();
            });

            $scope.foreachvar = null;
        } else {
            $ionicPopup.alert({
                template: 'Please select five traits',
                cssClass: 'adminpop',
            });
        }
    };


    $scope.submittechedit = function(monsterid) {
        var ref = firebase.database().ref();
        var monstersRef = ref.child("Monsters");

        if ($scope.monsters.collectorcount) {
            $scope.collectorcount = $scope.monsters.collectorcount
        } else {
            $scope.collectorcount = false
        }

        if ($scope.me) {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.me,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Technology'
            });
        } else {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.monsters.imgurl,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Technology'
            });
        }



        var alertPopup = $ionicPopup.alert({
            title: 'Changes Saved',
            template: 'Your technology status can be viewed on your Dreams page.'
        });


        alertPopup.then(function(res) {
            $scope.edittechmodal.hide();
        });
    };
    $scope.submitdescedit = function(monsterid) {
        var ref = firebase.database().ref();
        var monstersRef = ref.child("Monsters");

        if ($scope.monsters.collectorcount) {
            $scope.collectorcount = $scope.monsters.collectorcount
        } else {
            $scope.collectorcount = false
        }

        if ($scope.me) {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.me,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Discovery'
            });
        } else {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.monsters.imgurl,
                "rarity": $scope.monsters.rarity,
                "monetae": $scope.monsters.monetae,
                "type": $scope.monsters.type,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname,
                "catagory": 'Discovery'
            });
        }



        var alertPopup = $ionicPopup.alert({
            title: 'Changes Saved',
            template: 'Your discovery status can be viewed on your Dreams page.'
        });


        alertPopup.then(function(res) {
            $scope.editdescmodal.hide();
        });
    };

    $ionicModal.fromTemplateUrl('dreamerpop2.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.modal2 = modal;
    });

    $scope.openModal2 = function() {
        $scope.modal2.show();
    };

    $scope.closeModal2 = function() {
        $scope.modal2.hide();
    };

    $scope.closeeditModal = function() {
        $scope.editmodal.hide();
    };




    $ionicPopover.fromTemplateUrl('monsterdesc.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover1 = popover;
    });

    $ionicPopover.fromTemplateUrl('monsterurl.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover2 = popover;
    });

    $ionicPopover.fromTemplateUrl('monsterimgtype.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover3 = popover;
    });

    $scope.Monsterdesc = function($event) {
        $scope.popover1.show($event);
    };

    $scope.Monsterurl = function($event) {
        $scope.popover2.show($event);
    };

    $scope.Monsterimgtype = function($event) {
        $scope.popover3.show($event);
    };

    $scope.traitcreate = function() {
        $scope.maketraite = true;
    }

    $scope.untraitcreate = function() {
        $scope.maketraite = false;
    }

    $scope.recollect = function(data) {
        $scope.attackNum = data.traiteATK;
        $scope.defenseNum = data.traiteDEF;
        $scope.stunNum = data.traiteSTN;
    }


    $scope.traitsubmit = function(data) {

        var traiteref = firebase.database().ref().child("Traites");

        traiteref.once("value", function(snapshot) {
            var traiteID = snapshot.numChildren() + 1;

            if (traiteID && data.traiteName && data.traiteDesc && data.traiteATK && data.traiteDEF && data.traiteSTN && data.traiteEnv) {
                console.log("Traite # " + traiteID + " Added");

                traiteref.child(traiteID).set({
                    "Name": data.traiteName,
                    "Desc": data.traiteDesc,
                    "Attack": data.traiteATK,
                    "Defense": data.traiteDEF,
                    "Stun": data.traiteSTN,
                    "Enviroment": data.traiteEnv,
                    "approved": true,
                    "ID": traiteID
                });

                $scope.maketraite = false;

                $ionicPopup.alert({
                    template: 'Trait successfully submitted!',
                    cssClass: 'adminpop',
                });

            } else {
                $ionicPopup.alert({
                    template: 'Please make sure you fill everything out, a trait value is missing.',
                    cssClass: 'adminpop',
                });
            }
        });
    }

    $scope.traitcount = 0;

    $scope.refreshcounter = function() {
        $scope.traitcount = 0;
        var traits = $scope.traites;

        angular.forEach(traits, function(value, key) {
            $scope.traites[key].selected = false;
        });
    }

    var selectedtraits = [];

    $scope.selectcard = function(key) {

        if (!$scope.traites[key]) {
            key = 1;
        }

        if ($scope.traites[key]) {
            if ($scope.traites[key].selected) {
                $scope.alerttai = false;
                $scope.traites[key].selected = false;
                $scope.traitcount = $scope.traitcount - 1

                var index = selectedtraits.indexOf(key);
                selectedtraits.splice(index, 1);

            } else {
                if ($scope.traitcount <= 4) {
                    $scope.alerttai = false;
                    $scope.traites[key].selected = true;
                    $scope.traitcount = $scope.traitcount + 1

                    selectedtraits.push({
                        Name: $scope.traites[key].Name,
                        Desc: $scope.traites[key].Desc,
                        ATK: $scope.traites[key].Attack,
                        DEF: $scope.traites[key].Defense,
                        STN: $scope.traites[key].Stun,
                        Enviroment: $scope.traites[key].Enviroment,
                    });

                } else {
                    $scope.alerttai = true
                }
            }
        } else {
            $ionicPopup.alert({
                template: 'Error selecting card, please try again',
                cssClass: 'adminpop',
            });
        }

        $scope.traitsArray = selectedtraits;
        console.log($scope.traitsArray)
    }

    var traiteref = firebase.database().ref().child("Traites");
    $scope.traites = $firebaseArray(traiteref)


    var loadtraite = $scope.traites

    loadtraite.$loaded().then(function() {
        $scope.loadingtrait = true;
        console.log("TRAITS LOADED")
    });
})

.controller('SettingsCtrl', function($scope, $stateParams, $ionicModal, Monsters, $ionicPopover, $state, store, $ionicActionSheet, $ionicLoading, store, $ionicPopup, $firebaseAuth, $firebaseArray) {


    $ionicPopover.fromTemplateUrl('my-popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.openPopover = function($event) {
        $scope.popover.show($event);
    };

    var user = store.get('fbid');
    var puser = store.get('pwid');

    $scope.logout = function() {
        var hideSheet = $ionicActionSheet.show({
            destructiveText: 'Logout',
            titleText: 'Are you sure you want to logout of Venator?',
            cancelText: 'Cancel',
            cancel: function() {},
            buttonClicked: function(index) {
                return true;
            },
            destructiveButtonClicked: function() {
                $ionicLoading.show({
                    template: 'Logging out...'
                });

                if (puser) {

                    $scope.authObj = $firebaseAuth();
                    $scope.authObj.$signOut();

                    store.remove('pwid');
                    store.remove('fbid');
                    store.remove('fbname');
                    store.remove('pname');

                    $ionicLoading.hide();

                    $ionicPopup.alert({
                        template: 'Please close and re-open Venator',
                        cssClass: 'adminpop',
                    });

                    $state.go('login')
                } else {
                    facebookConnectPlugin.logout(function() {

                            store.remove('fbid');
                            store.remove('pwid');
                            store.remove('fbname');
                            store.remove('pname');

                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                template: 'Please close and re-open Venator',
                                cssClass: 'adminpop',
                            });

                            $state.go('login')
                        },
                        function(fail) {
                            $ionicLoading.hide();
                        });
                }
            }
        });
    };

    $ionicModal.fromTemplateUrl('tac.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.tac = modal;
    });

    $ionicModal.fromTemplateUrl('contr.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.contr = modal;
    });

    $scope.opencont = function() {
        $scope.contr.show();

        var helperref = firebase.database().ref().child("Helpers");
        $scope.helpers = $firebaseArray(helperref)

    };

    $scope.closecont = function() {
        $scope.contr.hide();
    };


    $scope.opentac = function() {
        $scope.tac.show();
    };

    $scope.closetac = function() {
        $scope.tac.hide();
    };

    $ionicModal.fromTemplateUrl('pp.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.pp = modal;
    });

    $scope.openpp = function() {
        $scope.pp.show();
    };

    $scope.closepp = function() {
        $scope.pp.hide();
    };

    $ionicModal.fromTemplateUrl('contact.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.con = modal;
    });

    $scope.opencon = function() {
        $scope.con.show();
    };

    $scope.closecon = function() {
        $scope.con.hide();
    };
})




.controller('AboutCtrl', function($scope, $stateParams, $ionicModal, Monsters, $ionicPopover, $state, $ionicSlideBoxDelegate, store, $ionicPopup, $firebaseArray, $firebaseObject) {
    $scope.data = {};

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.username = fbuser;
    } else {
        $scope.username = puser;
    }

    $ionicModal.fromTemplateUrl('adminreq.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.modal = modal;
    });


    $ionicModal.fromTemplateUrl('editpop.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.editmodal = modal;
    });

    $scope.showeditmodal = function(monster) {
        $scope.editmodal.show();

        $scope.monstername = monster.name
        $scope.monsters.name = monster.name
        $scope.monsters.desc = monster.desc
        $scope.monsters.enviroment = monster.enviroment
        $scope.monsters.imgurl = monster.imgurl
        $scope.monsters.CustomCss = monster.CustomCss
        $scope.monsters.artist = monster.artist
        $scope.monsters.rarity = monster.rarity
        $scope.monsters.collectorcount = monster.collectorcount
        $scope.monsterid = monster.id
    }

    $scope.submitedit = function(monsterid) {

        var ref = firebase.database().ref();
        var monstersRef = ref.child("Monsters");

        if ($scope.monsters.collectorcount) {
            $scope.collectorcount = $scope.monsters.collectorcount
        } else {
            $scope.collectorcount = false
        }

        if ($scope.me) {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.me,
                "rarity": $scope.monsters.rarity,
                "enviroment": $scope.monsters.enviroment,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname
            });
        } else {
            monstersRef.child(monsterid).set({
                "name": $scope.monsters.name,
                "desc": $scope.monsters.desc,
                "artist": $scope.monsters.artist,
                "imgurl": $scope.monsters.imgurl,
                "rarity": $scope.monsters.rarity,
                "enviroment": $scope.monsters.enviroment,
                'time': firebase.database.ServerValue.TIMESTAMP,
                // "imgtype": $scope.monsters.imgtype,
                "approved": 'pend',
                id: monsterid,
                "CustomCss": $scope.monsters.CustomCss,
                "collectorcount": $scope.collectorcount,
                "remcollectorcount": $scope.collectorcount,
                "user": $scope.displayname
            });
        }



        var alertPopup = $ionicPopup.alert({
            title: 'Changes Saved',
            template: 'Your monsters status can be viewed on your Dreams page.'
        });


        alertPopup.then(function(res) {
            $scope.editmodal.hide();
        });
    };


    $scope.approve = function(monster) {
        var rootRef = firebase.database().ref().child('Monsters');
        var membersRef = rootRef.child(monster.id);

        var objectToSave = {};
        var user_name = 'approved';
        objectToSave = true;

        membersRef.child(user_name).set(objectToSave);

        console.log('Changed Monsters Status to true')
    }

    $scope.disapprove = function(monster) {
        var rootRef = firebase.database().ref().child('Monsters');
        var membersRef = rootRef.child(monster.id);

        var objectToSave = {};
        var user_name = 'approved';
        objectToSave = false;
        membersRef.child(user_name).set(objectToSave);

        console.log('Changed Monsters Status to false')
    }

    $scope.init = function() {
        $ionicSlideBoxDelegate.update();
    };

    $scope.$on('$stateChangeSuccess', function() {
        $ionicSlideBoxDelegate.update();
    });



    $scope.adminlogin = function() {

        var alertPopup = $ionicPopup.alert({
            template: '<input type="password" placeholder="Enter Admin Password" ng-model="data.pass"></input> <div ng-if="fries == false" >Incorrect Password, try again</div>',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Exit'
            }, {
                text: 'Go',
                type: 'button-positive',
                onTap: function(e) {
                    if ($scope.data.pass == 'spj2016') {
                        $scope.modal.show();
                    } else {
                        e.preventDefault();
                        $scope.fries = false
                    }
                }
            }]
        });

    }

    $scope.closeModal = function() {
        $scope.modal.hide();
    };

    $scope.submitbutton = function() {
        $scope.modal.remove();
    };

    $scope.monsters = Monsters;

    var selectedUser;

    $scope.$on('$stateChangeSuccess', function() {
        $ionicSlideBoxDelegate.update();
    });

    $scope.updateUserInit = function(monster) {
        console.log("updateInit");

        monster.approved = 'yes';

        $ionicSlideBoxDelegate.update();
    }


    $scope.traitemode = function() {
        if ($scope.traits) {
            $scope.traits = false;
        } else {
            $scope.traits = true;
        }
    }

    $scope.approvetrait = function(traite) {
        console.log(traite)

        var traiteref = firebase.database().ref().child("Traites").child(traite.ID).child('approved');

        var traiteobj = $firebaseObject(traiteref);

        traiteobj.$loaded().then(function() {
            traiteobj.$value = true;
            traiteobj.$save()
        });
    }

    var traiteref = firebase.database().ref().child("Traites");
    $scope.traites = $firebaseArray(traiteref)

    $scope.back = function() {
        $state.go('app.settings')
    };

})

.controller('DropletsCtrl', function($scope, $firebaseArray, $ionicModal, $ionicPopup, store) {

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.username = fbuser;
    } else {
        $scope.username = puser;
    }


    var myfriendsref = firebase.database().ref().child("Codes");
    var monsterpull = myfriendsref.child($scope.username);

    $scope.usersRefs = $firebaseArray(monsterpull);

    $scope.usersRefs.$watch(function() {
        $scope.usersRefs = $firebaseArray(monsterpull);
    });


    $scope.remove = function(Monstercode, authData) {

        console.log($scope.Monstercode)

        var alertPopup = $ionicPopup.alert({
            template: 'Are you sure that you would like to set this creature free?',
            scope: $scope,
            cssClass: 'adminpop',
            buttons: [{
                text: 'Cancel'
            }, {
                text: 'Confirm',
                type: 'button-positive',
                onTap: function(e) {

                    var rootRef = firebase.database().ref().child('Codes/').child($scope.username).child($scope.Monstercode)


                    rootRef.remove();

                    var alertPopup = $ionicPopup.alert({
                        template: 'Creature Removed from Libary',
                        cssClass: 'adminpop',
                    });

                    alertPopup.then(function(res) {
                        $scope.amonsterselected = false;
                    });

                }
            }]
        });
    }

    $ionicModal.fromTemplateUrl('raritysearchmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.raritysearchmodal = modal;
    });

    $scope.openraritysearchmodal = function(Monstername, Monsterrarity) {
        $scope.raritysearchmodal.show();

        $scope.Monsterrarity = Monsterrarity;
        $scope.Monstername = Monstername;

        console.log(Monsterrarity)

    };

    $scope.closeraritysearchmodal = function() {
        $scope.raritysearchmodal.hide();
    };

    $scope.codes = Codes;

    $ionicModal.fromTemplateUrl('amonster.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.modal3 = modal;
    });


    $scope.openModal2 = function(usersRef) {
        $scope.modal3.show();

        $scope.Monstername = usersRef.Monster;
        $scope.Monsterimg = usersRef.MonsterImg;
        $scope.Monsterdesc = usersRef.MonsterDesc;
        $scope.Monstercode = usersRef.barcode;
        $scope.Monsterrarity = usersRef.MonsterRarity;
        $scope.Monsterenvo = usersRef.MonsterEnviroment;
        $scope.Monsterart = usersRef.MonsterArt;
        $scope.Monsterstatus = usersRef.MonsterStatus;
        $scope.Monsterworth = usersRef.MonsterWorth;
        $scope.customcss = usersRef.CustomCss;
        $scope.RemCount = usersRef.RemCollector;
        $scope.Count = usersRef.Collector;
    };

    $scope.closeModal2 = function() {
        $scope.modal3.hide();
    };

    $scope.doRefresh = function() {
        $timeout(function() {
            $scope.$broadcast('scroll.refreshComplete');
        }, 1500);
    };

})

.controller('BuggerCtrl', function($scope, $stateParams, $ionicPopup, $state, Bugs, store) {

    $scope.bugs = Bugs;

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.username = fbuser;
    } else {
        $scope.username = puser;
    }


    $scope.report = function() {
        var alertPopup = $ionicPopup.alert({
            title: 'Thank You!',
            template: 'We got your bug'
        });

        alertPopup.then(function(res) {
            $scope.bugs.$add({
                "bug": $scope.bugs.bug,
                "time": firebase.database.ServerValue.TIMESTAMP,
                "user": $scope.username
            });
        });
    };

})

.controller('OfflineCtrl', function($scope, $stateParams, $ionicHistory) {

    $ionicHistory.nextViewOptions({
        disableBack: true
    });

})


.controller('FriendCtrl', function($scope, $stateParams, $state, $ionicModal, $ionicPopup, Friends, $firebaseObject, $firebaseArray, $ionicLoading, store) {

    var connectedRef = firebase.database().ref().child(".info/connected");
    connectedRef.on("value", function(snap) {
        if (snap.val() === true) {
            $ionicLoading.hide().then(function() {
                $scope.offline = false
            });
        } else {
            $ionicLoading.hide().then(function() {
                $scope.offline = true
            });
        }
    });

    $scope.friends = Friends;

    var friendsRef = firebase.database().ref().child("Codes")

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.username = fbuser;
    } else {
        $scope.username = puser;
    }



    $ionicModal.fromTemplateUrl('raritysearchmodal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.raritysearchmodal = modal;
    });

    $scope.openraritysearchmodal = function(myfriend, corray) {

        if (corray) {
            $scope.leaderboardmodal.hide();
            $scope.raritysearchmodal.show();

            $scope.thepersonanme = null;

            var cccourtenr = firebase.database().ref().child("Codes/" + corray.id);
            $scope.personsarrays = $firebaseArray(cccourtenr);
        } else {
            $scope.leaderboardmodal.hide();
            $scope.raritysearchmodal.show();

            var cccourtenr = firebase.database().ref().child("Codes/" + myfriend.id);
            $scope.personsarrays = $firebaseArray(cccourtenr);
        }

        $scope.thepersonanme = myfriend.visiablename;
        console.log($scope.thepersonanme)
    };

    var ref = firebase.database().ref().child("Codes/");
    var hopref = ref.child($scope.username)

    var results = $firebaseArray(hopref);

    results.$loaded(function(x) {
        $scope.results = results;
        $scope.notloadingloadedbit = true;
    });


    $scope.isUserMonster = function(personsarray) {
        var fianalresult = $scope.results;
        var lresult
        var userExists = true;

        angular.forEach(fianalresult, function(value, key) {
            if (userExists) {
                if (value.Monster == personsarray.Monster) {
                    lresult = true;
                    userExists = false;
                } else {
                    lresult = false;
                }
            }

        });

        return lresult;

    }

    $scope.closeraritysearchmodal = function() {
        $scope.raritysearchmodal.hide();

        $scope.thepersonanme = null;
        $scope.personsarrays = null;
    };

    var myfriendsref = firebase.database().ref().child("Codes");
    var myfriendsref2 = myfriendsref.child($scope.username);
    var myfriendsref3 = myfriendsref2.child("Friends");

    $scope.myfriends = $firebaseArray(myfriendsref3);
    $scope.hiddenarray = $firebaseArray(myfriendsref);

    myfriendsref2.once("value", function(snapshot) {
        $scope.friends.creaturecount = snapshot.numChildren();

        $scope.hiddenarray.$watch(function() {
            if ($scope.friends.creaturecount >= 6) {
                $scope.access = true
            } else {
                $scope.access = false
            }
        });
    });


    $ionicModal.fromTemplateUrl('codemodal.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.codemodal = modal;
    });


    $scope.opencodemodal = function() {
        $scope.codemodal.show();

        var isuser1 = firebase.database().ref().child("Codes/");
        var isuser2 = isuser1.child($scope.myusername);

        var isuser5 = isuser2.child("User Information");

        var theotherproviderRef = isuser5.child("provider");

        var friendprovider = new $firebaseObject(theotherproviderRef);

        if (friendprovider.$value == 'password') {
            $scope.thefriendvarref = 'email'
        } else {
            $scope.thefriendvarref = 'username'
        }

        var isuser6 = isuser5.child($scope.thefriendvarref);

        var isuser7 = $firebaseObject(isuser6);

        isuser7.$loaded().then(function() {
            $scope.finalisuser = isuser7.$value
        });
    };

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.myusername = fbuser;
    } else {
        $scope.myusername = puser;
    }


    var isuser1 = firebase.database().ref().child("Codes/");
    var isuser2 = isuser1.child($scope.myusername);
    var isuser3 = isuser2.child("Friends");

    var isuser4 = $firebaseArray(isuser3);

    isuser4.$loaded(function(x) {
        $scope.theresults = isuser4;
    });



    $scope.HaveFriend = function(friendsRef, authData) {

        if (friendsRef.provider == 'facebook') {
            $scope.emailorname = friendsRef.username;
        } else {
            $scope.emailorname = friendsRef.email;
        }

        var lastresult = $scope.theresults;
        var lresult
        var userExists = false;

        angular.forEach(lastresult, function(value, key) {
            if (!userExists) {
                if (value.visiablename == $scope.emailorname) {
                    lresult = true;
                    userExists = true;
                } else {
                    lresult = false;
                }
            }
        });
        return lresult;
    }

    $scope.closecodemodal = function() {
        $scope.codemodal.hide();
    };

    $ionicModal.fromTemplateUrl('battlemodal.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.battlemodal = modal;
    });

    $ionicModal.fromTemplateUrl('leaderboard.html', {
        scope: $scope,
        animation: 'scale-in'
    }).then(function(modal) {
        $scope.leaderboardmodal = modal;
    });


    $scope.leaderboard = function() {
        $scope.leaderboardmodal.show();

        var leaderboardref = firebase.database().ref().child("Codes");
        var newarrays = $firebaseArray(leaderboardref)

        var arr = [];

        newarrays.$loaded().then(function() {
            angular.forEach(newarrays, function(value, key) {

                var cccourtenr = firebase.database().ref().child("Codes/" + value.$id + "/User Information");
                var courtenr = cccourtenr.child("email")
                var courtenr2 = cccourtenr.child("username")
                var cbunter = cccourtenr.child("id")


                var specobj = $firebaseObject(courtenr)
                var specobj2 = $firebaseObject(courtenr2)
                var cbobj = $firebaseObject(cbunter)

                specobj.$loaded().then(function() {
                    if (specobj.$value) {
                        $scope.mnusername = specobj.$value
                    } else {
                        $scope.mnusername = specobj2.$value
                    }

                    $scope.userid = cbobj.$value;

                    var keys = Object.keys(value);
                    var len = keys.length;
                    var oglen = len - 4

                    if (oglen < 0) {
                        $scope.finallen = 0
                    } else {
                        $scope.finallen = oglen
                    }

                    arr.push({
                        Count: $scope.finallen,
                        Username: $scope.mnusername,
                        id: $scope.userid,
                    });
                });
            });
        });

        $scope.corrays = arr;

        console.log($scope.corrays)
    };

    $scope.closeleaderboard = function() {
        $scope.leaderboardmodal.hide();
    };

    $scope.battle = function(myfriend) {
        $scope.battlemodal.show();
        $scope.doug = false;

        $scope.Friendname = myfriend.visiablename;

        $scope.Friendid = myfriend.id;

        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.username = fbuser;
        } else {
            $scope.username = puser;
        }

        var refa = firebase.database().ref();
        var refb = refa.child("Codes");
        var refc = refb.child($scope.username);
        var refd = refc.child("Friends");
        var refe = refd.child(myfriend.id);

        var reff = refe.child('enemymonsterrarity');
        var refg = new $firebaseObject(reff);

        refg.$loaded().then(function() {
            refg.$watch(function() {
                $scope.EnemyRarity = myfriend.enemymonsterrarity;
                $scope.enemyspin = true
            });
        });

        var refh = refe.child('enemymonsterimage');
        var refi = new $firebaseObject(refh);

        refi.$loaded().then(function() {
            refi.$watch(function() {
                $scope.EnemyImage = myfriend.enemymonsterimage;
            });
        });

        var refj = refe.child('mymonsterrarity');
        var refk = new $firebaseObject(refj);

        refk.$loaded().then(function() {
            refk.$watch(function() {
                $scope.MyRarity = myfriend.mymonsterrarity;
                $scope.myspin = true
            });


        });

        var refl = refe.child('mymonsterimage');
        var refm = new $firebaseObject(refl);

        refi.$loaded().then(function() {
            refm.$watch(function() {
                $scope.MyImage = myfriend.mymonsterimage;
            });
        });


        $scope.EnemyRarity = myfriend.enemymonsterrarity;
        $scope.EnemyImage = myfriend.enemymonsterimage;
        $scope.EnemyImageShiny = myfriend.enemyimageshiny;
        $scope.EnemyImageCss = myfriend.enemyimagecss;

        $scope.MyImage = myfriend.mymonsterimage;
        $scope.MyImageShiny = myfriend.myimageshiny;
        $scope.MyImageCss = myfriend.myimagecss;
        $scope.MyRarity = myfriend.mymonsterrarity;

        if ($scope.MyRarity == 'Common') {
            $scope.playernumber = 1
        } else if ($scope.MyRarity == 'Uncommon') {
            $scope.playernumber = 2
        } else if ($scope.MyRarity == 'Rare') {
            $scope.playernumber = 3
        } else if ($scope.MyRarity == 'Super Rare') {
            $scope.playernumber = 4
        } else if ($scope.MyRarity == 'Mysterious') {
            $scope.playernumber = 5
        } else {
            $scope.playernumber = 0
        }

        if ($scope.EnemyRarity == 'Common') {
            $scope.enemynumber = 1
        } else if ($scope.EnemyRarity == 'Uncommon') {
            $scope.enemynumber = 2
        } else if ($scope.EnemyRarity == 'Rare') {
            $scope.enemynumber = 3
        } else if ($scope.EnemyRarity == 'Super Rare') {
            $scope.enemynumber = 4
        } else if ($scope.EnemyRarity == 'Mysterious') {
            $scope.enemynumber = 5
        } else {
            $scope.enemynumber = 0
        }

        console.log($scope.playernumber + " VS " + $scope.playernumber)
        if ($scope.playernumber == 0) {
            $scope.BattleState = "noplayer";
        } else if ($scope.enemynumber == 0) {
            $scope.BattleState = "noenemy";
        } else if (angular.equals($scope.playernumber, $scope.enemynumber)) {

            $scope.BattleState = "draw";

            var alertPopup2 = $ionicPopup.alert({
                template: 'You have drawn, and both keep your monsters',
                cssClass: 'adminpop',
            });
        } else if ($scope.playernumber >= $scope.enemynumber) {

            $scope.BattleState = "won";

            var fbuser = store.get('fbid');
            var puser = store.get('pwid');

            if (fbuser) {
                $scope.username = fbuser;
            } else {
                $scope.username = puser;
            }

            var ref = firebase.database().ref();
            var codesRef = ref.child("Codes");
            var usersRef = codesRef.child(myfriend.id);

            var ref2 = firebase.database().ref();
            var codes2Ref = ref2.child("Codes");
            var users2Ref = codes2Ref.child($scope.username);

            var codeRef = usersRef.child(myfriend.enemymonsterbarcode);
            var code2Ref = users2Ref.child(myfriend.enemymonsterbarcode);

            var monterbarcodenameRef = codeRef.child('Monster');
            var barcodenameobj = new $firebaseObject(monterbarcodenameRef);

            var montertimenameRef = codeRef.child('time');
            var barcodetimeobj = new $firebaseObject(montertimenameRef);

            var monsterload1Ref = users2Ref.child(myfriend.mymonsterbarcode);
            var monsterload2Ref = monsterload1Ref.child('Monster');
            var monsterload3Ref = new $firebaseObject(monsterload2Ref);

            monsterload3Ref.$watch(function() {
                if (barcodenameobj.$value == null) {} else {
                    codeRef.once('value', function(snapshot) {
                        var alertPopup2 = $ionicPopup.alert({
                            template: 'Congratulations, you have won. You have obtained ' + barcodenameobj.$value,
                            cssClass: 'adminpop',
                        });
                        alertPopup2.then(function(res) {
                            code2Ref.set(snapshot.val(), function(error) {
                                if (!error) {
                                    codeRef.remove();
                                } else if (typeof(console) !== 'undefined' && console.error) {
                                    console.error(error);
                                }
                            });
                            barcodetimeobj.$value = firebase.database.ServerValue.TIMESTAMP;
                            barcodetimeobj.$save();
                        });
                    });
                }
            });
        } else if ($scope.playernumber <= $scope.enemynumber) {

            $scope.BattleState = "lost";

            var alertPopup2 = $ionicPopup.alert({
                template: 'Sorry, you have lost this battle, and your creature.',
                cssClass: 'adminpop',
            });
        } else {
            $scope.BattleState = "none";
        }

        $scope.battleroyale = function() {
            $scope.battle(myfriend)
        };

    };



    $scope.closebattlemodal = function() {
        $scope.battlemodal.hide();
    };

    $scope.draw = function() {
        var alertPopup = $ionicPopup.alert({
            template: 'You have drawn, nothing has happened and you both keep your monster.',
            cssClass: 'adminpop',
        });
    };

    $scope.won = function() {
        var alertPopup = $ionicPopup.alert({
            template: 'Congratulations, you have won. You have obtained the enemies creature.',
            cssClass: 'adminpop',
        });
    };

    $scope.lost = function() {
        var alertPopup = $ionicPopup.alert({
            template: 'Sorry, you have lost. You have lost the creature that fought.',
            cssClass: 'adminpop',
        });
    };

    // var MyMonster = firebase.database().ref().child("Codes/" + $scope.username);
    // var results = $firebaseArray(MyMonster);
    // var everyone = []

    // results.$loaded(function(x) {

    //     var lopperres = results;

    //     angular.forEach(lopperres, function(value, key) {
    //         if (value.favourite) {

    //             everyone.push({
    //                 image: value.MonsterImg,
    //                 barcode: value.barcode,
    //                 name: value.Monster,
    //                 rarity: value.MonsterRarity,
    //                 level: value.Monsterlvl,
    //                 customcss: value.CustomCss
    //             });
    //         }
    //     });

    //     $scope.oneResult = results[Math.floor(Math.random() * $scope.friends.count)];
    //     console.log(everyone)

    //     $scope.myfavrepeats = everyone
    // });


    $scope.spin = function(Friendid) {
        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.username = fbuser;
        } else {
            $scope.username = puser;
        }

        var ref = firebase.database().ref();
        var codesRef = ref.child("Codes");
        var usersRef = codesRef.child($scope.username);

        usersRef.once("value", function(snapshot) {
            $scope.friends.count = snapshot.numChildren();
        });

        var MyMonster = firebase.database().ref().child("Codes/" + $scope.username);

        // var rint = Math.floor((Math.random() * $scope.friends.count-2))

        // var query = MyMonster.orderByChild("index").startAt(rint).limitToFirst(1);

        var results = $firebaseArray(MyMonster);

        results.$loaded(
            function(x) {

                $scope.oneResult = results[Math.floor(Math.random() * $scope.friends.count)];

                console.log($scope.oneResult)

                var ref = firebase.database().ref();
                var codesRef = ref.child("Codes");
                var usersRef = codesRef.child($scope.username);
                var friendsRef = usersRef.child("Friends");
                var friendobj = friendsRef.child(Friendid);

                var finalobj = friendobj.child('mymonsterimage');

                var latestobj = new $firebaseObject(finalobj);

                latestobj.$value = $scope.oneResult.MonsterImg;
                latestobj.$save();

                var final2obj = friendobj.child('mymonsterrarity');

                var latest2obj = new $firebaseObject(final2obj);


                latest2obj.$value = $scope.oneResult.MonsterRarity;
                latest2obj.$save();

                var barcodeobj = friendobj.child('mymonsterbarcode');

                var barcodecoreobj = new $firebaseObject(barcodeobj);

                barcodecoreobj.$value = $scope.oneResult.barcode;
                barcodecoreobj.$save();

                var final901obj = friendobj.child('mymonstershiny');

                var latest901obj = new $firebaseObject(final901obj);

                latest901obj.$value = $scope.oneResult.Shiny;
                latest901obj.$save();

                var final101obj = friendobj.child('myimagecss');

                var latest101obj = new $firebaseObject(final101obj);

                latest101obj.$value = $scope.oneResult.CustomCss;
                latest101obj.$save();


                var users5Ref = codesRef.child(Friendid);
                var friends5Ref = users5Ref.child("Friends");
                var friend5obj = friends5Ref.child($scope.username);
                var final5obj = friend5obj.child('enemymonsterimage');

                var latest3obj = new $firebaseObject(final5obj);

                latest3obj.$value = $scope.oneResult.MonsterImg;
                latest3obj.$save();

                var final7obj = friend5obj.child('enemymonsterrarity');

                var latest9obj = new $firebaseObject(final7obj);

                var final900obj = friend5obj.child('enemymonstershiny');

                var latest900obj = new $firebaseObject(final900obj);

                latest900obj.$value = $scope.oneResult.Shiny;
                latest900obj.$save();

                var final100obj = friend5obj.child('enemyimagecss');

                var latest100obj = new $firebaseObject(final100obj);

                latest100obj.$value = $scope.oneResult.CustomCss;
                latest100obj.$save();


                var barcode2obj = friend5obj.child('enemymonsterbarcode');

                var barcode2coreobj = new $firebaseObject(barcode2obj);

                barcode2coreobj.$value = $scope.oneResult.barcode;
                barcode2coreobj.$save();

                if (angular.equals(latestobj.$value, latest3obj.$value)) {
                    console.log('Set MyMonster to: ' + latestobj.$value + ' With a rarity of ' + latest2obj.$value);
                } else {
                    var alertPopup = $ionicPopup.alert({
                        template: 'There seems to be some sort of an error',
                        cssClass: 'adminpop',
                    });
                }

            },
            function(error) {
                console.error("Error:", error);
            });

    };

    $scope.rematch = function(Friendid) {
        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.username = fbuser;
        } else {
            $scope.username = puser;
        }

        var ref = firebase.database().ref();
        var codesRef = ref.child("Codes");
        var usersRef = codesRef.child($scope.username);
        var friendsRef = usersRef.child("Friends");
        var friendobj = friendsRef.child(Friendid);
        var finalobj = friendobj.child('mymonsterimage');

        var latestobj = new $firebaseObject(finalobj);

        latestobj.$value = false;
        latestobj.$save();

        var final2obj = friendobj.child('mymonsterrarity');

        var latest2obj = new $firebaseObject(final2obj);

        latest2obj.$value = false;
        latest2obj.$save();

        var final45obj = friendobj.child('enemymonsterimage');

        var latest45obj = new $firebaseObject(final45obj);

        latest45obj.$value = false;
        latest45obj.$save();

        var final25obj = friendobj.child('enemymonsterrarity');

        var latest25obj = new $firebaseObject(final25obj);

        latest25obj.$value = false;
        latest25obj.$save();

        var users5Ref = codesRef.child(Friendid);
        var friends5Ref = users5Ref.child("Friends");
        var friend5obj = friends5Ref.child($scope.username);
        var final5obj = friend5obj.child('enemymonsterimage');

        var latest3obj = new $firebaseObject(final5obj);

        latest3obj.$value = false;
        latest3obj.$save();

        var final7obj = friend5obj.child('enemymonsterrarity');

        var latest9obj = new $firebaseObject(final7obj);

        latest9obj.$value = false;
        latest9obj.$save();

        var final900obj = friend5obj.child('enemymonstershiny');

        var latest900obj = new $firebaseObject(final900obj);

        latest900obj.$value = false;
        latest900obj.$save();

        var final100obj = friend5obj.child('enemyimagecss');

        var latest100obj = new $firebaseObject(final100obj);

        latest100obj.$value = false;
        latest100obj.$save();


        var final56obj = friend5obj.child('mymonsterimage');

        var latest56obj = new $firebaseObject(final56obj);

        latest56obj.$value = false;
        latest56obj.$save();

        var final71obj = friend5obj.child('mymonsterrarity');

        var latest71obj = new $firebaseObject(final71obj);

        latest71obj.$value = false;
        latest71obj.$save();

        var final901obj = friend5obj.child('mymonstershiny');

        var latest901obj = new $firebaseObject(final901obj);

        latest901obj.$value = false;
        latest901obj.$save();

        var final101obj = friend5obj.child('myimagecss');

        var latest101obj = new $firebaseObject(final101obj);

        latest101obj.$value = false;
        latest101obj.$save();

        if (angular.equals(latestobj.$value, latest3obj.$value)) {
            console.log('Set MyMonster to: ' + latestobj.$value + ' With a rarity of ' + latest2obj.$value);
        } else {
            var alertPopup = $ionicPopup.alert({
                template: 'There seems to be some sort of an error',
                cssClass: 'adminpop'
            });
        }
    };

    $scope.trade = function() {
        var alertPopup = $ionicPopup.alert({
            template: 'Trading is coming to Venator soon...',
            cssClass: 'adminpop'
        });
    };


    $scope.addfriend = function(friendsRef) {

        $scope.thefriendprovider = friendsRef.provider;

        if ($scope.thefriendprovider == 'password') {
            $scope.thefriendname = friendsRef.email;
        } else {
            $scope.thefriendname = friendsRef.username;
        }

        $scope.thefriendid = friendsRef.id;
        $scope.playerpoints = friendsRef.PlayerPoints;

        if (friendsRef.id == undefined) {
            var alertPopup = $ionicPopup.alert({
                template: 'Error adding Friend. User needs to have the latest version and logout and log back in again.',
                cssClass: 'adminpop'
            });
        } else {



            console.log($scope.thefriendname + " " + $scope.thefriendid + " " + $scope.thefriendvarref);

            var fbuser = store.get('fbid');
            var puser = store.get('pwid');

            if (fbuser) {
                $scope.username = fbuser;
            } else {
                $scope.username = puser;
            }

            var ref = firebase.database().ref();
            var codesRef = ref.child("Codes");
            var usersRef = codesRef.child($scope.username);
            var friendsRef = usersRef.child("Friends");

            friendsRef.child($scope.thefriendid).once('value', function(snapshot) {

                var exists = (snapshot.val() !== null);

                if (snapshot.exists()) {
                    var alertPopup = $ionicPopup.alert({
                        template: 'You already have this friend.',
                        cssClass: 'adminpop'
                    });
                } else {
                    friendsRef.child($scope.thefriendid).set({
                        visiablename: $scope.thefriendname,
                        id: $scope.thefriendid,
                        mymonsterimage: false,
                        mymonsterrarity: false,
                        mymonsterbarcode: false,
                        myimageshiny: false,
                        myimagecss: false,
                        enemymonsterimage: false,
                        enemymonsterrarity: false,
                        enemymonsterbarcode: false,
                        enemyimageshiny: false,
                        enemyimagecss: false,
                        battlestate: false,
                    });

                    var alertPopup = $ionicPopup.alert({
                        template: 'Friend Added',
                        cssClass: 'adminpop'
                    });
                    alertPopup.then(function(res) {
                        $scope.codemodal.hide();
                    });


                    var users2Ref = codesRef.child($scope.thefriendid);
                    var friends2Ref = users2Ref.child("Friends");

                    var friendinfo2Ref = usersRef.child("User Information");
                    var theotherproviderRef = friendinfo2Ref.child("provider");

                    var friendprovider = new $firebaseObject(theotherproviderRef);

                    if (friendprovider.$value == 'password') {
                        $scope.thefriendvarref = 'email'
                    } else {
                        $scope.thefriendvarref = 'username'
                    }

                    var friendinfo3Ref = friendinfo2Ref.child($scope.thefriendvarref);

                    var friendauthobj = new $firebaseObject(friendinfo3Ref);

                    friendauthobj.$loaded().then(function() {

                        friends2Ref.child($scope.username).set({
                            visiablename: friendauthobj.$value,
                            id: $scope.username,
                            mymonsterimage: false,
                            mymonsterrarity: false,
                            mymonsterbarcode: false,
                            myimageshiny: false,
                            myimagecss: false,
                            enemymonsterimage: false,
                            enemymonsterrarity: false,
                            enemyimageshiny: false,
                            enemyimagecss: false,
                            enemymonsterbarcode: false,
                            battlestate: false
                        }, function(error) {});

                    });
                }
            });
        };
    }
})

.controller('PlanetCtrl', function($scope, $stateParams, $ionicPopup, $ionicModal, $state, $firebaseObject, store, $firebaseArray, $ionicLoading, $timeout, $interval, $ionicHistory) {
    $ionicModal.fromTemplateUrl('planeta.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.planeta = modal;
    });

    $ionicModal.fromTemplateUrl('stationsheet.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.stationsheet = modal;
    });

    var isinbattle = store.get('isinBattle');

    if (isinbattle) {
        $ionicHistory.clearCache().then(function() {
            $state.go('tab.battleAI');
        });
    }


    $scope.closea = function() {
        $scope.planeta.hide();
    };

    $scope.closesheet = function() {
        $scope.stationsheet.hide();

        $scope.tradeselected = false;
        $scope.giftmode = false;
        $scope.acceptgiftmode = false;
    };

    var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, 'phaser-example');

    var PhaserGame = function() {

        this.sprite;

        this.pad;

        this.stick;
        this.buttonC;

    };

    PhaserGame.prototype = {

        init: function() {
            this.game.sound.stopAll();

            this.game.renderer.renderSession.roundPixels = true;
            this.physics.startSystem(Phaser.Physics.ARCADE);
            $scope.phaserloaded = true;


            var fbuser = store.get('fbid');
            var puser = store.get('pwid');

            if (fbuser) {
                $scope.usersusername = fbuser;
            } else {
                $scope.usersusername = puser;
            }

            console.log($scope.usersusername)
            var ref = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/montae");
            var montaeref = new $firebaseObject(ref);
            this.montae = montaeref.$value
        },

        preload: function() {

            this.load.atlas('generic', 'img/joystick.png', 'joystick.json');
            this.load.image('ship', 'img/ship.png');

            this.load.image('planeta', 'img/planets/1.png', 64, 64);
            this.load.image('planetb', 'img/planets/2.png', 64, 64);
            this.load.image('planetc', 'img/planets/3.png', 64, 64);
            this.load.image('planetd', 'img/planets/4.png', 64, 64);
            this.load.image('planete', 'img/planets/5.png', 64, 64);
            this.load.image('planetf', 'img/planets/6.png', 64, 64);
            this.load.image('planetg', 'img/planets/7.png', 64, 64);
            this.load.image('planeth', 'img/planets/8.png', 132, 132);
            this.load.image('planeti', 'img/planets/9.png', 132, 132);

            this.load.image('enemy1', 'img/enemies/1.png');
            this.load.image('enemy2', 'img/enemies/2.png');
            this.load.image('enemy3', 'img/enemies/3.png');
            this.load.image('enemy4', 'img/enemies/4.png');
            this.load.image('enemy5', 'img/enemies/5.png');
            this.load.image('enemy6', 'img/enemies/6.png');
            this.load.image('enemy7', 'img/enemies/7.png');

            this.load.image('bg', 'img/starrysky.png');
            this.load.image('speaker', 'img/speaker.png');
            this.load.image('speakerc', 'img/speakerc.png');

            this.load.image('enemyfoundtxt', 'img/shipfoundtxt.png');

            this.load.image('station', 'img/station.png');

            this.time.advancedTiming = true;
        },

        create: function() {
            this.sky = this.add.image(0, 0, 'bg');
            this.sky.anchor.set(0.5);

            this.speaker = this.add.image(0, 0, 'speaker');
            this.speakerc = this.add.image(0, 0, 'speakerc');
            this.station = this.add.image(0, 0, 'station');

            this.enemyfoundtxt = this.add.image(0, 0, 'enemyfoundtxt');

            this.stage.backgroundColor = '#131313';
            // scaling
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.maxWidth = this.game.width;
            this.scale.maxHeight = this.game.height;
            this.scale.pageAlignHorizontally = true;

            this.planeta = this.game.add.physicsGroup();
            this.planetb = this.game.add.physicsGroup();
            this.planetc = this.game.add.physicsGroup();
            this.planetd = this.game.add.physicsGroup();
            this.planete = this.game.add.physicsGroup();
            this.planetf = this.game.add.physicsGroup();
            this.planetg = this.game.add.physicsGroup();
            this.planeth = this.game.add.physicsGroup();
            this.planeti = this.game.add.physicsGroup();

            var randomS2 = Math.random() * (2500 - (-2500)) + (-2500);
            var randomS3 = Math.random() * (2500 - (-2500)) + (-2500);

            this.sprite = this.add.sprite(randomS2 * 10, randomS3 * 10, 'ship');

            this.sprite.texture.baseTexture.scaleMode = PIXI.NEAREST;
            this.sprite.scale.set(0.2);
            this.sprite.anchor.set(0.5);
            this.physics.arcade.enableBody(this.sprite);
            this.physics.arcade.enable(this.sprite);

            this.pad = this.game.plugins.add(Phaser.VirtualJoystick);

            this.stick = this.pad.addStick(this.game.width/6.5, this.game.height/1.2, 30, 'generic');
            this.stick.scale = this.game.width/550;

            this.buttonC = this.pad.addButton(this.game.width/1.1, this.game.height/1.2, 'generic', 'button1-up', 'button1-down');
            this.buttonC.onDown.add(this.pressButtonC, this);
            this.buttonC.scale = this.game.width/450;
            this.buttonC.anchor  = 0.5;
            console.log(this.game.width);

            this.camera.follow(this.sprite, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);

            this.sky.y = Math.floor(this.sprite.y);
            this.sky.x = Math.floor(this.sprite.x);
            this.sky.height = this.game.height;
            this.sky.width = this.game.width;

            this.speaker.x = this.game.width + 5;
            this.speaker.y = 300;
            this.speaker.height = 200;
            this.speaker.width = 130;
            this.speaker.angle += 90;
            this.speaker.fixedToCamera = true;
            this.speaker.bringToTop()

            this.speakerc.x = this.game.width - 58;
            this.speakerc.y = 366;
            this.speakerc.height = 110;
            this.speakerc.width = 110;
            this.speakerc.fixedToCamera = true;
            this.speakerc.anchor.setTo(0.5, 0.5);
            this.speakerc.inputEnabled = true;
            this.speakerc.events.onInputDown.add(this.pressCompass, this);
            this.speakerc.bringToTop()

            this.enemyfoundtxt.x = this.game.width - 580;
            this.enemyfoundtxt.y = 1090;
            this.enemyfoundtxt.height = 100;
            this.enemyfoundtxt.width = 470;
            this.enemyfoundtxt.fixedToCamera = true;
            this.enemyfoundtxt.visible = false;
            this.enemyfoundtxt.bringToTop()


            this.station.x = 0;
            this.station.y = 0;
            this.station.height = 573;
            this.station.width = 1275;
            this.station.anchor.setTo(0.5, 0.5);

            var sprite = this.sprite
            var planeta = this.planeta

            $scope.movement = 0;
        },

        update: function() {
            this.world.setBounds(-this.sprite.yChange, -this.sprite.xChange, this.game.height + this.sprite.yChange, this.game.width + this.sprite.xChange);

            var maxSpeed = 500;

            if (this.enemy) {
                var distance = 2;

                this.enemy.x += distance * Math.cos(this.enemy.angle * Math.PI / 180);
                this.enemy.y += distance * Math.sin(this.enemy.angle * Math.PI / 180);

            }

            if (this.stick.isDown) {
                this.physics.arcade.velocityFromRotation(this.stick.rotation, this.stick.force * maxSpeed, this.sprite.body.velocity);
                this.sprite.rotation = this.stick.rotation;

                this.sky.y = Math.floor(this.sprite.y);
                this.sky.x = Math.floor(this.sprite.x);

                this.speakerc.angle = this.sprite.x / 5;

                this.spawnPlanets();
                this.spawnEnemies();

            } else {
                this.sprite.body.velocity.set(0);

                if ($scope.recenter) {
                    $scope.recenter = false;
                    this.sprite.x = 0;
                    this.sprite.y = 0;
                    this.sky.y = 0;
                    this.sky.x = 0;
                }
            }

        },
        spawnPlanets: function() {

            this.whichToSpawn = Math.floor(Math.random() * (3000 - 1) + 1);

            this.spawnRate = Math.floor(Math.random() * (1000 - (-1000)) + (-1000));
            this.spawnRateY = Math.floor(Math.random() * (1000 - (-1000)) + (-1000));

            var pointa = this.sprite.x + this.spawnRate;
            var pointb = this.sprite.y + this.spawnRateY;

            if (this.whichToSpawn == 1) {
                this.planeta.create(pointa, pointb, 'planeta', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planeta.scale.x = this.scale;
                this.planeta.scale.y = this.scale;
            } else if (this.whichToSpawn == 2) {
                this.planetb.create(pointa, pointb, 'planetb', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planetb.scale.x = this.scale;
                this.planetb.scale.y = this.scale;
            } else if (this.whichToSpawn == 3) {
                this.planetc.create(pointa, pointb, 'planetc', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planetc.scale.x = this.scale;
                this.planetc.scale.y = this.scale;
            } else if (this.whichToSpawn == 4) {
                this.planetd.create(pointa, pointb, 'planetd', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planetd.scale.x = this.scale;
                this.planetd.scale.y = this.scale;
            } else if (this.whichToSpawn == 5) {
                this.planete.create(pointa, pointb, 'planete', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planete.scale.x = this.scale;
                this.planete.scale.y = this.scale;
            } else if (this.whichToSpawn == 6) {
                this.planetf.create(pointa, pointb, 'planetf', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planetf.scale.x = this.scale;
                this.planetf.scale.y = this.scale;
            } else if (this.whichToSpawn == 7) {
                this.planetg.create(pointa, pointb, 'planetg', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planetg.scale.x = this.scale;
                this.planetg.scale.y = this.scale;
            } else if (this.whichToSpawn == 8) {
                this.planeth.create(pointa, pointb, 'planeth', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planeth.scale.x = this.scale;
                this.planeth.scale.y = this.scale;
            } else if (this.whichToSpawn == 9) {
                this.planeti.create(pointa, pointb, 'planeti', 0);

                this.scale = game.rnd.integerInRange(2, 0.5);
                this.planeti.scale.x = this.scale;
                this.planeti.scale.y = this.scale;
            }

        },
        spawnEnemies: function() {

            var lookforenemy = false;
            var caninterval = true;


            this.spawnEnemiesCtrl();
            this.enemyfoundtxt.visible = false;

        },
        spawnEnemiesCtrl() {

            this.whichToSpawn = Math.floor(Math.random() * (50 - 1) + 1);
            // console.log("Trying to spawn w/ " + this.whichToSpawn)

            this.spawnRate = Math.floor(Math.random() * (1000 - (-1000)) + (-1000));
            this.spawnRateY = Math.floor(Math.random() * (1000 - (-1000)) + (-1000));

            var pointa = this.sprite.x + this.spawnRate;
            var pointb = this.sprite.y + this.spawnRate;

            if (!$scope.enemystopper && this.whichToSpawn == 2) {

                this.enemyfoundtxt.visible = true;
                $scope.spawnControl = $scope.spawnControl + 1

                if(this.enemy && this.enemy.visible){
                  this.enemy.visible = false;
                  this.enemy.alive = false;
                }else{
                  var rand = Math.round(Math.random() * (7 - 1) + 1);
                  this.enemy = this.add.sprite(pointa, pointb, 'enemy' + rand);

                  console.log("Enemy " + rand + " Spawned!")
                }


                var scale = game.rnd.integerInRange(1, 0.1);
                var rotation = game.rnd.integerInRange(180, 1);

                this.enemy.scale.set(scale);
                this.enemy.anchor.set(0.5);
                this.enemy.angle += rotation;

                $scope.movement = $scope.movement + 1
                $scope.enemystopper = true;

                setTimeout(function(){
                  $scope.enemystopper = false;
                }, 1500);
            }
        },
        shutdown: function() {
            // reset everything, or the world will be messed up
            this.world.setBounds(0, 0, this.game.width, this.game.height);
            this.platforms.destroy();
            this.platforms = null;
        },

        // checkOverlap: function(planeta, sprite) {
        //     console.log(planeta)
        //     var boundsA = planeta.getBounds();
        //     var boundsB = sprite.getBounds();

        //     return Phaser.Rectangle.intersects(boundsA, boundsB);
        // },

        render: function() {

            if (!this.stick.force) {
                this.speed = 'DEACTIVATED';
            } else {
                this.speed = 'ACTIVATED';
            }

            this.allPlanetCount = this.planeta.length + this.planetb.length + this.planetc.length + this.planetd.length + this.planete.length + this.planetf.length + this.planetg.length + this.planeth.length + this.planeti.length;

            if (this.allPlanetCount == 10) {
                this.planeta.visable = false;
                this.planetb.visable = false;
                this.planetc.visable = false;
                this.planetd.visable = false;
                this.planete.visable = false;
                this.planetf.visable = false;
                this.planetg.visable = false;
                this.planeth.visable = false;
                this.planeti.visable = false;
            }

            game.debug.text("Starship Coordinates : x " + Math.floor(this.sprite.x) + ", y " + Math.floor(this.sprite.y), 32, 50);
            game.debug.text("Starship  Activated : " + this.speed, 32, 80);
            game.debug.text("Detected Planets : " + Math.floor(this.allPlanetCount), 32, 120);
            game.debug.text("Detected Enemies : " + $scope.movement, 32, 145);

            var ref = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/montae");
            var montaeref = new $firebaseObject(ref);

            montaeref.$loaded().then(function() {
                $scope.monteamount = montaeref.$value
            });

            if ($scope.monteamount) {
                this.montetext = game.debug.text("Montae:☫" + $scope.monteamount, this.game.width - 150, 50, '#FFA631');
            } else if ($scope.monteamount == 0) {
                game.debug.text("Montae:0", this.game.width - 150, 50, '#FFA631');
            }

            if (this.time.fps < 10) {
                game.debug.text("FPS : " + this.time.fps || '--', 32, 175, "#D24D57");
            } else if (this.time.fps < 29) {
                game.debug.text("FPS : " + this.time.fps || '--', 32, 175, "#F9690E");
            } else if (this.time.fps > 30) {
                game.debug.text("FPS : " + this.time.fps || '--', 32, 175, "#26C281");
            }
        },
        pressCompass: function() {
            $ionicPopup.alert({
                template: 'Are you sure that you would like to travel to the center of the universe?',
                scope: $scope,
                cssClass: 'adminpop',
                buttons: [{
                    text: 'Cancel'
                }, {
                    text: 'Confirm',
                    type: 'button-positive',
                    onTap: function(e) {
                        $scope.recenter = true;
                    }
                }]
            });
        },
        pressButtonC: function() {

            if (this.sprite.overlap(this.station)) {

                $ionicLoading.show({
                    template: '<ion-spinner class="spinner" icon="lines"></ion-spinner>'
                })

                var fbuser = store.get('fbid');
                var puser = store.get('pwid');

                if (fbuser) {
                    $scope.usersusername = fbuser;
                } else {
                    $scope.usersusername = puser;
                }

                var peopleref = firebase.database().ref().child("Codes").child($scope.usersusername);
                $scope.monsterarrays = $firebaseArray(peopleref);
                var monsterarray = $firebaseArray(peopleref);

                monsterarray.$loaded().then(function() {

                    $scope.monstercount = 0;

                    angular.forEach(monsterarray, function(value, key) {
                        if (value.Monster && value.Monsterlvl >= 1 && value.MonsterEnviroment && !value.Traded) {
                            $scope.monstercount = $scope.monstercount + 1
                        }
                    });

                    $timeout(function() {
                        console.log($scope.monstercount)

                        $ionicLoading.hide();

                        $scope.tradeselected = false;
                        $scope.stationsheet.show();
                    }, 100);
                });

            } else if (this.physics.arcade.overlap(this.sprite, this.planeta)) {
                store.set('choosenPlanet', 'planeta')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planetb)) {
                store.set('choosenPlanet', 'planetb')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planetc)) {
                store.set('choosenPlanet', 'planetc')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planetd)) {
                store.set('choosenPlanet', 'planetd')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planete)) {
                store.set('choosenPlanet', 'planete')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planetf)) {
                store.set('choosenPlanet', 'planetf')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planetg)) {
                store.set('choosenPlanet', 'planetg')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planeth)) {
                store.set('choosenPlanet', 'planeth')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.physics.arcade.overlap(this.sprite, this.planeti)) {
                store.set('choosenPlanet', 'planeti')
                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.planetWalk');
                });
            } else if (this.sprite.overlap(this.enemy)) {
                this.enemyfoundtxt.visible = false;
                store.set('choosenEnemy', this.enemy.key)

                $ionicHistory.clearCache().then(function() {
                    $state.go('tab.battleAI');
                });
            } else {
                var alertPopup = $ionicPopup.alert({
                    template: 'Nothing found...',
                    cssClass: 'adminpop'
                });
            }
        }

    };
    game.state.add('Game', PhaserGame, true);

    $scope.viewplanet = function() {
        $scope.planeta.hide();
    };

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    $scope.addstate = function() {
        if (fbuser) {
            $scope.usableplanet = fbuser;
        } else {
            $scope.usableplanet = puser;
        }

        console.log($scope.usableplanet + $scope.originplanet);

        var ref = firebase.database().ref().child("Codes/" + $scope.usableplanet + "/User Information/planet");
        var planetRef = new $firebaseObject(ref);

        var ref2 = firebase.database().ref().child("Codes/" + $scope.usableplanet + "/User Information/english");
        var englishref = new $firebaseObject(ref2);

        englishref.$value = $scope.english;
        englishref.$save();

        planetRef.$value = $scope.originplanet;
        planetRef.$save();

        $ionicPopup.alert({
            template: 'Your chances of catching ' + $scope.english + ' Creatures has increased.',
            cssClass: 'scanemptypop'
        });
    };

    if (fbuser) {
        $scope.usableplanet = fbuser;
    } else {
        $scope.usableplanet = puser;
    }

    var ref = firebase.database().ref().child("Codes/" + $scope.usableplanet + "/User Information/planet");
    var planetRef = new $firebaseObject(ref);

    planetRef.$watch(function() {
        $scope.myplanet2 = planetRef.$value;
    });

    planetRef.$loaded(function() {
        $scope.myplanet2 = planetRef.$value;
    });

    $scope.backHome = function() {
        $scope.giftmode = false;
        $scope.acceptgiftmode = false;
        $scope.tradeselected = false;

    };

    $scope.tradestation = function(monsterarray) {
        $scope.showh2 = false;


        $timeout(function() {
            $scope.showh2 = true;
        }, 500);

        $scope.tradeselected = true;

        $scope.giftmode = false;
        $scope.acceptgiftmode = false;
        $scope.showcaughtcreature = false;

        $scope.selectedMonster = monsterarray.Monster
        $scope.selectedMonsterImage = monsterarray.MonsterImg
        $scope.selectedMonsterRarity = monsterarray.MonsterRarity
        $scope.selectedMonsterDesc = monsterarray.MonsterDesc
        $scope.selectedMonsterCustomCss = monsterarray.CustomCss
        $scope.selectedMonsterShiny = monsterarray.shiny
        $scope.selectedMonsterCode = monsterarray.barcode
    };

    $scope.tradeexchange = function(monteamount) {
        $scope.showcaughtcreature = false;

        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.usersusername = fbuser;
        } else {
            $scope.usersusername = puser;
        }

        if (monteamount >= 50 && monteamount <= 200) {
            $scope.tselectedMonsterRarity = 'Common'
            $scope.montaetolose = 50;
        } else if (monteamount >= 201 && monteamount <= 500) {
            $scope.tselectedMonsterRarity = 'Uncommon'
            $scope.montaetolose = 200;
        } else if (monteamount >= 501 && monteamount <= 800) {
            $scope.tselectedMonsterRarity = 'Rare'
            $scope.montaetolose = 500;
        } else if (monteamount >= 801 && monteamount <= 1000) {
            $scope.tselectedMonsterRarity = 'Super Rare'
            $scope.montaetolose = 800;
        } else if (monteamount >= 1001) {
            $scope.tselectedMonsterRarity = 'Mysterious'
            $scope.montaetolose = 1000;
        }

        var selectedMonsterRarity = $scope.tselectedMonsterRarity
        console.log(selectedMonsterRarity)

        var ref = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/montae");
        var montaeref = new $firebaseObject(ref);

        montaeref.$loaded(function() {
            montaeref.$value = montaeref.$value - $scope.montaetolose;
            montaeref.$save();
        });

        $scope.tradeselected = true;
        $scope.unlockCreature(selectedMonsterRarity)
    };

    $scope.CancelISSTrade = function() {
        $scope.tradeselected = false
        $scope.giftmode = false;
        $scope.acceptgiftmode = false;
    };


    $scope.ReleaseISSTrade = function(selectedMonsterCode) {
        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.usersusername = fbuser;
        } else {
            $scope.usersusername = puser;
        }

        var monstersref = firebase.database().ref().child("Monsters");
        var monsterref = firebase.database().ref().child("Codes").child($scope.usersusername).child(selectedMonsterCode);

        monsterref.remove();

        console.log(selectedMonsterCode)

        var alertPopup = $ionicPopup.alert({
            template: 'Creature Lost. Gift Gained.',
            cssClass: 'adminpop',
        });

        var monster = $firebaseArray(monsterref)

        monster.$loaded().then(function() {
            console.log(selectedMonsterCode)
        });

        $scope.giftmode = true;

    };

    $scope.unlockCreature = function(selectedMonsterRarity) {




        var fbuser = store.get('fbid');
        var puser = store.get('pwid');

        if (fbuser) {
            $scope.usersusername = fbuser;
        } else {
            $scope.usersusername = puser;
        }


        var monstersref = firebase.database().ref().child("Monsters")

        var randomarraymon = $firebaseArray(monstersref)

        var randommonster = [];

        randomarraymon.$loaded().then(function() {
            angular.forEach(randomarraymon, function(value, key) {

                if (value.rarity == selectedMonsterRarity && value.enviroment && value.approved == true) {
                    randommonster.push({
                        id: value.$id
                    });
                }
            });

            $scope.rand = randommonster[Math.floor(Math.random() * randommonster.length)];

            console.log(randommonster)

            var monstersRef = firebase.database().ref().child("Monsters").child($scope.rand.id);
            var monsterrefarr = $firebaseObject(monstersRef);

            var usersRef = firebase.database().ref().child("Codes").child($scope.usersusername);

            monsterrefarr.$loaded().then(function() {
                $scope.giftmode = false;
                $scope.acceptgiftmode = true;

                $scope.randomlevel = Math.round(Math.random() * (15 - 1) + 1);
                console.log($scope.randomlevel)

                $scope.unlockedCreature = monsterrefarr.name
                $scope.unlockedDesc = monsterrefarr.desc
                $scope.unlockedRare = monsterrefarr.rarity
                $scope.unlockedImage = monsterrefarr.imgurl
                $scope.unlockedLevel = $scope.randomlevel
                $scope.unlockedCustomCSS = monsterrefarr.CustomCss

                var randomnum = Math.floor(Math.random() * 2000000000000);

                usersRef.child(randomnum).set({
                    barcode: randomnum,
                    user: $scope.usersusername,
                    time: firebase.database.ServerValue.TIMESTAMP,
                    favourite: false,
                    Monster: monsterrefarr.name,
                    MonsterImg: monsterrefarr.imgurl,
                    MonsterDesc: monsterrefarr.desc,
                    MonsterRarity: monsterrefarr.rarity,
                    MonsterArt: monsterrefarr.artist,
                    MonsterEnviroment: monsterrefarr.enviroment,
                    MonsterStatus: monsterrefarr.approved,
                    CustomCss: monsterrefarr.CustomCss,
                    Monsterlvl: $scope.randomlevel,
                    Traded: true,
                    Catagory: 'Creature'
                });

                $scope.showcaughtcreature = true;
            });
        });
    };
})


.controller('EventsCtrl', function($scope, $state, $firebaseArray, store, $timeout) {

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    if (fbuser) {
        $scope.usersusername = fbuser;
    } else {
        $scope.usersusername = puser;
    }

    var monstersref = firebase.database().ref().child("Monsters");
    var peopleref = firebase.database().ref().child("Codes").child($scope.usersusername);

    var newarrays = $firebaseArray(monstersref)
    var newcodearrs = $firebaseArray(peopleref)

    var eventarray = [];
    var mycreatures = [];

    newcodearrs.$loaded().then(function() {
        angular.forEach(newcodearrs, function(value, key) {
            if (value.RemCollector) {
                mycreatures.push({
                    Name: value.Monster
                });
            }
        });
    });

    newarrays.$loaded().then(function() {
        angular.forEach(newarrays, function(value, key) {
            if (value.collectorcount) {

                $timeout(function() {
                    angular.forEach(mycreatures, function(value, key) {
                        $scope.mycreature = value.Name
                    });

                    $scope.runouttime = value.time + 8000000000;

                    console.log($scope.runouttime)

                    if ($scope.mycreature == value.name) {
                        eventarray.push({
                            Mine: true,
                            RemColl: value.remcollectorcount,
                            Coll: value.collectorcount,
                            imgurl: value.imgurl,
                            name: value.name,
                            approved: value.approved,
                            time: $scope.runouttime
                        });
                    } else {
                        eventarray.push({
                            Mine: false,
                            RemColl: value.remcollectorcount,
                            Coll: value.collectorcount,
                            imgurl: value.imgurl,
                            name: value.name,
                            approved: value.approved,
                            time: $scope.runouttime
                        });
                    }

                    $scope.corrays = eventarray;
                }, 1000);


            }
        });
    });

})

.controller('planetWalkCtrl', function($scope, $state, store, $ionicPopup, $firebaseObject, $firebaseArray) {
    var planet = store.get('choosenPlanet')
    console.log(planet);

    var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, 'planetwalk');


    var PhaserGame = function() {

        this.sprite;

        this.pad;

        this.stick;

        this.buttonA;
        this.buttonB;
        this.buttonC;
    };

    PhaserGame.prototype = {

        init: function() {

            this.game.renderer.renderSession.roundPixels = true;
            this.physics.startSystem(Phaser.Physics.ARCADE);

        },

        preload: function() {

            this.load.atlas('generic', 'img/joystick.png', 'joystick.json');
            this.load.image('ship', 'img/ship.png');

            this.load.image('moonbg', 'img/planets/moonfloor.png');
            this.load.image('floor2', 'img/planets/floor2.jpg');
            this.load.image('floor3', 'img/planets/floor3.png');
            this.load.image('floor4', 'img/planets/floor4.png');
            this.load.image('floor5', 'img/planets/floor5.png');
            this.load.image('floor6', 'img/planets/floor6.png');
            this.load.image('floor7', 'img/planets/floor7.png');
            this.load.image('floor8', 'img/planets/floor8.png');
            this.load.image('floor9', 'img/planets/floor9.png');

            this.load.image('plant1', 'img/fauna/1.png');
            this.load.image('plant2', 'img/fauna/2.png');
            this.load.image('plant3', 'img/fauna/3.png');
            this.load.image('plant4', 'img/fauna/4.png');
            this.load.image('plant5', 'img/fauna/5.png');
            this.load.image('plant6', 'img/fauna/6.png');
            this.load.image('plant7', 'img/fauna/7.png');
            this.load.image('plant8', 'img/fauna/8.png');
            this.load.image('plant9', 'img/fauna/9.png');
            this.load.image('plant10', 'img/fauna/10.png');
            this.load.image('plant11', 'img/fauna/11.png');
            this.load.image('plant12', 'img/fauna/12.png');

            this.load.image('rock1', 'img/fauna/rock1.png');
            this.load.image('rock2', 'img/fauna/rock2.png');
            this.load.image('rock3', 'img/fauna/rock3.png');
            this.load.image('rock4', 'img/fauna/rock4.png');

            this.load.image('rock1', 'img/fauna/rock1.png');
            this.load.image('rock2', 'img/fauna/rock2.png');
            this.load.image('rock3', 'img/fauna/rock3.png');
            this.load.image('rock4', 'img/fauna/rock4.png');

            this.load.image('trait1', 'img/fauna/trait1.png');
            this.load.image('trait2', 'img/fauna/trait2.png');
            this.load.image('trait3', 'img/fauna/trait3.png');
            this.load.image('trait4', 'img/fauna/trait4.png');
            this.load.image('trait5', 'img/fauna/trait5.png');
            this.load.image('trait6', 'img/fauna/trait6.png');

            this.load.image('greenTower', 'img/objects/space/tower.png');
            this.load.image('spaceTanks', 'img/objects/space/tanks.png');
            this.load.image('spaceMachine', 'img/objects/space/machine.png');
            this.load.image('spaceSucker', 'img/objects/space/sucker.png');

            this.load.image('treasure', 'img/treasure.png');

            this.load.image('flag', 'img/flag.png');

            this.load.spritesheet('spaceman', 'img/planets/spaceman.png', 37, 45, 12);
            // this.load.spritesheet('hunredtrees', 'img/fauna/hunredtrees.png', 40, 40, 10);


            $scope.phaserloaded = true;

        },

        create: function() {
            if (planet == 'planeta') {
                this.add.tileSprite(0, 0, 1920, 1920, 'moonbg');
            } else if (planet == 'planetb') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor2');
            } else if (planet == 'planetc') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor3');
            } else if (planet == 'planetd') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor4');
            } else if (planet == 'planete') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor5');
            } else if (planet == 'planetf') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor6');
            } else if (planet == 'planetg') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor7');
            } else if (planet == 'planeth') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor8');
            } else if (planet == 'planeti') {
                this.add.tileSprite(0, 0, 1920, 1920, 'floor9');
            }

            this.world.setBounds(0, 0, 1920, 1920);


            // scaling
            this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.scale.maxWidth = this.game.width;
            this.scale.maxHeight = this.game.height;
            this.scale.pageAlignHorizontally = true;


            this.ship = this.add.sprite(200, 200, 'ship');
            this.ship.scale.set(0.3);
            this.ship.anchor.set(0.5);
            this.ship.angle += 32;
            this.physics.arcade.enableBody(this.ship);
            this.physics.arcade.enable(this.ship);

            this.sprite = this.add.sprite(400, 400, 'spaceman');

            this.walk = this.sprite.animations.add('walk', [0, 1, 2])
            this.left = this.sprite.animations.add('left', [3, 4, 5])
            this.right = this.sprite.animations.add('right', [6, 7, 8])
            this.up = this.sprite.animations.add('up', [9, 10, 11])

            this.sprite.texture.baseTexture.scaleMode = PIXI.NEAREST;
            this.sprite.scale.set(2);
            this.sprite.anchor.set(0.5);
            this.sprite.enableBody = true;
            this.sprite.physicsBodyType = Phaser.Physics.ARCADE;
            this.sprite.collideWorldBounds = true;
            this.physics.arcade.enable(this.sprite);
            this.physics.enable(this.sprite, Phaser.Physics.ARCADE);

            this.camera.follow(this.sprite, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);

            this.pad = this.game.plugins.add(Phaser.VirtualJoystick);


            this.stick = this.pad.addStick(this.game.width/6.5, this.game.height/1.2, 30, 'generic');
            this.stick.scale = this.game.width/550;

            this.buttonC = this.pad.addButton(this.game.width/1.1, this.game.height/1.2, 'generic', 'button1-up', 'button1-down');
            this.buttonC.onDown.add(this.pressButtonC, this);
            this.buttonC.scale = this.game.width/450;
            this.buttonC.anchor  = 0.5;

            // var hunredtreesGroup = game.add.group();
            // hunredtreesGroup.scale.set(2);


            if (planet == 'planeta') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

                //Objects
                this.greenTower = this.game.add.physicsGroup();
                this.spaceTanks = this.game.add.physicsGroup();
                this.spaceMachine = this.game.add.physicsGroup();
                this.spaceSucker = this.game.add.physicsGroup();

                this.greenTower.create(Math.random() * (1700 - 0) + 0, Math.random() * (1200 - 0) + 0, 'greenTower');
                this.spaceTanks.create(Math.random() * (1700 - 0) + 0, Math.random() * (1200 - 0) + 0, 'spaceTanks');
                this.spaceMachine.create(Math.random() * (1700 - 0) + 0, Math.random() * (1200 - 0) + 0, 'spaceMachine');
                this.spaceSucker.create(Math.random() * (1700 - 0) + 0, Math.random() * (1200 - 0) + 0, 'spaceSucker');

                this.spaceMachine.scale.x = 0.3;
                this.spaceMachine.scale.y = 0.3;
                this.spaceSucker.scale.x = 0.4;
                this.spaceSucker.scale.y = 0.4;



            } else if (planet == 'planetb') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocksrocks = Math.random() * (3 - 1) + 1;

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plants = this.game.add.physicsGroup();
                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (5 - 1) + 1;
                var random5 = Math.random() * (3 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant6');
                }

                for (var i = 0; i < Math.floor(random5); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant8');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

            } else if (planet == 'planetc') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocks = Math.random() * (3 - 1) + 1;

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (5 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant2');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

            } else if (planet == 'planetd') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocks = Math.floor(Math.random() * (3 - 1) + 1);

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (5 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant3');
                    this.plants.create(random2, random3, 'plant4');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

            } else if (planet == 'planete') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocks = Math.random() * (3 - 1) + 1;

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plant1 = this.game.add.physicsGroup();

                var random1 = Math.random() * (5 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plant1.scale.x = randomScale;
                    this.plant1.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plant1.create(random2, random3, 'plant1');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

            } else if (planet == 'planetf') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocks = Math.random() * (3 - 1) + 1;

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plant7 = this.game.add.physicsGroup();

                var random1 = Math.random() * (5 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plant7.scale.x = randomScale;
                    this.plant7.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plant7.create(random2, random3, 'plant7');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }

            } else if (planet == 'planetg') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                var randomrocks = Math.random() * (3 - 1) + 1;

                this.rocks = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomrocks); i++) {
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock1');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock2');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock3');
                    this.rocks.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'rock4');
                }

                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (3 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant9');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }
            } else if (planet == 'planeth') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (10 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;
                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant12');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }
            } else if (planet == 'planeti') {

                var randomtrait = Math.floor(Math.random() * (2 - 0) + 0);

                this.traits = this.game.add.physicsGroup();

                for (var i = 0; i < Math.floor(randomtrait); i++) {
                    var traitpicker = Math.floor(Math.random() * (6 - 1) + 1);
                    this.traits.create(Math.random() * (1800 - 0) + 0, Math.random() * (1800 - 0) + 0, 'trait' + traitpicker);
                }

                this.plants = this.game.add.physicsGroup();

                var random1 = Math.random() * (10 - 1) + 1;

                for (var i = 0; i < Math.floor(random1); i++) {

                    var randomScale = Math.random() * (1 - 0.7) + 0.7;

                    this.plants.scale.x = randomScale;
                    this.plants.scale.y = randomScale;

                    var random2 = Math.random() * (1920 - 0) + 0;
                    var random3 = Math.random() * (1920 - 0) + 0;

                    var random4 = Math.random() * (1920 - 0) + 0;
                    var random5 = Math.random() * (1920 - 0) + 0;

                    this.plants.create(random2, random3, 'plant10');
                    this.plants.create(random4, random5, 'plant11');
                }

                this.treasure = this.game.add.physicsGroup();
                var randomt = Math.random() * (3 - 1) + 1;

                if (Math.floor(randomt) == 2) {
                    console.log("Treasure Found")
                    var treasurex = Math.random() * (1920 - 0) + 0;
                    var treasurey = Math.random() * (1920 - 0) + 0;
                    this.treasure.create(treasurex, treasurey, 'treasure');
                } else {
                    console.log("No Treasure Found, " + Math.floor(randomt))
                }
            }

        },
        pressButtonC: function() {

            var fbuser = store.get('fbid');
            var puser = store.get('pwid');

            if (fbuser) {
                $scope.usersusername = fbuser;
            } else {
                $scope.usersusername = puser;
            }

            if (this.physics.arcade.overlap(this.sprite, this.ship)) {
                store.set('choosenPlanet', '')
                $state.go('tab.planet')
            } else if (this.physics.arcade.overlap(this.sprite, this.traits)) {


                var traiteRef = firebase.database().ref().child("Traites");
                var traiteFinalRef = $firebaseArray(traiteRef);
                var pickedtraits = [];

                traiteFinalRef.$loaded(function() {

                if (planet == 'planeta') {
                    var planetname = 'space'
                } else if (planet == 'planetb') {
                    var planetname = 'city'
                } else if (planet == 'planetc') {
                    var planetname = 'sea'
                } else if (planet == 'planetd') {
                    var planetname = 'forest'
                } else if (planet == 'planete') {
                    var planetname = 'desert'
                } else if (planet == 'planetf') {
                    var planetname = 'plains'
                } else if (planet == 'planetg') {
                    var planetname = 'swamp'
                } else if (planet == 'planeth') {
                    var planetname = 'neutral'
                } else if (planet == 'planeti') {
                    var planetname = 'neutral'
                }

                    angular.forEach(traiteFinalRef, function(val) {
                        if (planetname == val.Enviroment) {
                            pickedtraits.push({
                                name: val.Name,
                                desc: val.Desc,
                                attack: val.Attack,
                                defense: val.Defense,
                                stun: val.Stun
                            })
                        }
                    });

                    var finalTrait = pickedtraits[Math.floor(Math.random() * pickedtraits.length)];


                    if (finalTrait) {
                        $ionicPopup.alert({
                            template: "<img class='popupimageplanet' src='img/planets/" + planetname + ".png'><span class='liltext'> " + planetname + " Traite </span> <h3> " + finalTrait.name + " </h3> <div class='traitstats'><div class='row'><div class='col'>ATK " + finalTrait.attack + "</div><div class='col'>DEF" + finalTrait.defense + "</div><div class='col'>STN " + finalTrait.stun + "</div></div></div><h4> " + finalTrait.desc + "</h4>",
                            cssClass: 'adminpop'
                        });
                    } else {
                        $ionicPopup.alert({
                            template: "Problem Collecting Trait. Please try again later.",
                            cssClass: 'adminpop'
                        });
                    }
                });

                this.traits.visible = false;


            } else if (this.physics.arcade.overlap(this.sprite, this.treasure)) {
                var randomAmount = Math.random() * (100 - 1) + 1;

                var Monteeeref = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/montae");
                var montaeref = new $firebaseObject(Monteeeref);

                montaeref.$loaded(function() {
                    if (!montaeref.$value) {
                        montaeref.$value = Math.floor(randomAmount);
                        montaeref.$save();
                    } else {
                        montaeref.$value = montaeref.$value + Math.floor(randomAmount);
                        montaeref.$save();
                    }

                    this.montae = montaeref.$value

                    $ionicPopup.alert({
                        template: "☫" + Math.floor(randomAmount) + " Montae uncovered",
                        cssClass: 'adminpop'
                    });

                });

                this.treasure.visible = false;

            } else {
                this.sprite.animations.play('walk', 5);
                this.flag = this.add.sprite(this.sprite.x, this.sprite.y, 'flag');
                this.flag.scale.set(1.5);

                if (planet == 'planeta') {
                    this.planetname = 'space'
                    this.officalname = 'Locus'
                } else if (planet == 'planetb') {
                    this.planetname = 'city'
                    this.officalname = 'Urbs'
                } else if (planet == 'planetc') {
                    this.planetname = 'sea'
                    this.officalname = 'Mare'
                } else if (planet == 'planetd') {
                    this.planetname = 'forest'
                    this.officalname = 'Silva'
                } else if (planet == 'planete') {
                    this.planetname = 'desert'
                    this.officalname = 'Solitudinem'
                } else if (planet == 'planetf') {
                    this.planetname = 'plains'
                    this.officalname = 'Campis'
                } else if (planet == 'planetg') {
                    this.planetname = 'swamp'
                    this.officalname = 'Palus'
                } else if (planet == 'planeth') {
                    this.planetname = 'mysterious'
                    this.officalname = 'Arcanum'
                } else if (planet == 'planeti') {
                    this.planetname = 'rare'
                    this.officalname = 'Rara'
                }


                var ref2 = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/english");
                var ref3 = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/planet");
                var englishref = new $firebaseObject(ref2);
                var englishref2 = new $firebaseObject(ref3);

                englishref.$value = this.planetname;
                englishref.$save();

                englishref2.$value = this.officalname;
                englishref2.$save();


                englishref.$loaded(function() {
                    $scope.currentplanetname = englishref.$value;
                });


                if (this.planetname != $scope.currentplanetname) {
                    $ionicPopup.alert({
                        template: "<img class='popupimageplanet' src='img/planets/" + this.planetname + ".PNG'> <h4> You have landed on the planet " + this.officalname + ". Your chances of catching " + this.planetname + " creatures has increased</h4>",
                        cssClass: 'adminpop'
                    });
                } else {
                    $ionicPopup.alert({
                        template: "You are settled on " + this.officalname,
                        cssClass: 'adminpop'
                    });
                }

            }

        },

        render: function() {
            var ref = firebase.database().ref().child("Codes/" + $scope.usersusername + "/User Information/montae");
            var montaeref = new $firebaseObject(ref);

            montaeref.$loaded().then(function() {
                $scope.monteamount = montaeref.$value
            });

            if ($scope.monteamount) {
                this.montetext = game.debug.text("Montae:☫" + $scope.monteamount, this.game.width - 150, 50, '#FFA631');
            } else if ($scope.monteamount == 0) {
                game.debug.text("Montae:0", this.game.width - 150, 50, '#FFA631');
            }
        },
        update: function() {
            var maxSpeed = 400;

            if (this.stick.isDown) {
                this.physics.arcade.velocityFromRotation(this.stick.rotation, this.stick.force * maxSpeed, this.sprite.body.velocity);

                if (this.stick.quadrant == 0) {
                    this.sprite.animations.play('right', 5);
                } else if (this.stick.quadrant == 3) {
                    this.sprite.animations.play('up', 5);
                } else if (this.stick.quadrant == 1) {
                    this.sprite.animations.play('walk', 5);
                } else if (this.stick.quadrant == 2) {
                    this.sprite.animations.play('left', 5);
                }

                // console.log(this.sprite.x + ", " + this.sprite.y)

            } else {
                this.sprite.body.velocity.set(0);
            }

            if (this.sprite.x < 0) {
                this.sprite.x = 1
            } else if (this.sprite.x > 1920) {
                this.sprite.x = 1919
            } else if (this.sprite.y < 0) {
                this.sprite.y = 1
            } else if (this.sprite.y > 1800) {
                this.sprite.y = 1799
            }

        }

    };

    game.state.add('Game', PhaserGame, true);

})

.controller('battleAICtrl', function($scope, $firebaseArray, store, $ionicPopup, $ionicHistory, $state, $timeout, $firebaseObject) {
    var storeenemyimage = store.get('choosenEnemy');

    var fbuser = store.get('fbid');
    var puser = store.get('pwid');

    var fname = store.get('fbname');
    var pwname = store.get('pname');``

    if (fbuser) {
        $scope.username = fbuser;
        $scope.displayname = fname;
    } else {
        $scope.username = puser;
        $scope.displayname = pwname;
    }

    $scope.isloadingindivudals = true;

    var myCreaturesRef = firebase.database().ref().child("Codes/").child($scope.username)
    var allmycreatures = $firebaseArray(myCreaturesRef);

    allmycreatures.$loaded(function(x) {
        $scope.isloadingindivudals = true;
    });

    if (storeenemyimage == 'enemy1') {
        $scope.enemyimage = 1
        $scope.shipName = 'The Vaharmis'
    } else if (storeenemyimage == 'enemy2') {
        $scope.enemyimage = 2
        $scope.shipName = 'Daha Union'
    } else if (storeenemyimage == 'enemy3') {
        $scope.enemyimage = 3
        $scope.shipName = 'CorasLap'
    } else if (storeenemyimage == 'enemy4') {
        $scope.enemyimage = 4
        $scope.shipName = 'Daha Union'
    } else if (storeenemyimage == 'enemy5') {
        $scope.enemyimage = 5
        $scope.shipName = 'Veep Territories'
    } else if (storeenemyimage == 'enemy6') {
        $scope.enemyimage = 6
        $scope.shipName = 'Hapor Nasior'
    } else if (storeenemyimage == 'enemy7') {
        $scope.enemyimage = 7
        $scope.shipName = 'Corgorga Cell'
    }

    var monstersRef = firebase.database().ref().child("Monsters");
    var allMonsters = $firebaseArray(monstersRef);

    allMonsters.$loaded(function(x) {
        var coreid = store.get('isinBattle');

        if (!coreid) {
            console.log("Creature Reset")

            var setbattlelvl = Math.floor(Math.random() * (25 - 2 + 1) + 2);
            store.set('isinBattlelvl', setbattlelvl);

            console.log("Enemy Level " + setbattlelvl);
            $scope.AIMaxLevel = setbattlelvl;

            var teHealth = setbattlelvl * 50;
            store.set('enemyhp', teHealth);
            $scope.totalHealth = teHealth;
            $scope.maxehp = teHealth;

            console.log("ENEMY HP SET TO " + teHealth)

            var eatk = Math.floor(Math.random() * ((setbattlelvl*10) - (setbattlelvl/2) + 1) + (setbattlelvl/2));
            store.set('eatk', eatk);
            $scope.eatk = eatk;

            var edef = Math.floor(Math.random() * ((setbattlelvl*10) - (setbattlelvl/2) + 1) + (setbattlelvl/2));
            store.set('edef', edef);
            $scope.edef = edef;

            var estn = Math.floor(Math.random() * ((setbattlelvl*10) - (setbattlelvl/2) + 1) + (setbattlelvl/2));
            store.set('estn', estn);
            $scope.estn = estn;


            store.set('playermode', 1);

            store.set('hp', 0);

            store.set('myatk', 0);
            store.set('mydef', 0);
            store.set('mystn', 0);

            store.set('creatureName', 0);
            store.set('creatureImage', 0);
            store.set('creatureCustomCss', 0);
            store.set('creatureShiny', 0);
            store.set('creatureLvl', 0);
            store.set('creatureRarity', 0);
            store.set('creatureCode', 0);

            store.set('isinBattleEsc', true);

            $scope.playermode = 1;

            var approvedIDS = [];

            angular.forEach(allMonsters, function(val) {
                if(val.approved == true){
                  approvedIDS.push(val)
                }
            });

            console.log(approvedIDS)
            var setbattlekey = Math.floor(Math.random() * approvedIDS.length)
            var rand = approvedIDS[setbattlekey];

            store.set('randomMonsterImage', rand.imgurl);
            store.set('randomCustomCss', rand.CustomCss);
            store.set('randomMonsterName', rand.name);
            store.set('randomMonsterRarity', rand.rarity);
        }



        $scope.randomMonsterImage = store.get('randomMonsterImage');
        $scope.randomCustomCss = store.get('randomCustomCss');
        $scope.randomMonsterName = store.get('randomMonsterName');
        $scope.randomMonsterRarity = store.get('randomMonsterRarity');


        $scope.Level = store.get('creatureLvl');

        $scope.AIMaxLevel = store.get('isinBattlelvl');

        var eHealth = store.get('enemyhp');
        $scope.totalHealth = eHealth;

        $scope.myHealth = store.get('hp');


        $scope.eatk = store.get('eatk');
        $scope.edef = store.get('edef');
        $scope.estn = store.get('estn');

        $scope.myatk = store.get('myatk');
        $scope.mydef = store.get('mydef');
        $scope.mystn = store.get('mystn');

        $scope.myHealth = store.get('hp');

    });


    var playermode = store.get('playermode');

    if (!playermode) {
        store.set('playermode', 1);
    }

    $scope.playermode = playermode;

    $scope.canescape = store.get('isinBattleEsc');

    $scope.attemptFlea = function() {
        var chances = Math.floor(Math.random() * ($scope.AIMaxLevel - 1) + 1);

        console.log("Escape Roll: " + chances)
        if (chances == $scope.AIMaxLevel - 1) {
            $ionicPopup.alert({
                template: 'Escape Successful',
                cssClass: 'adminpop'
            });

            $ionicHistory.clearCache().then(function() {
                $state.go('tab.planet');
            });

            store.set('isinBattle', false);
            store.set('isinBattlelvl', false);
            store.set('isinBattleEsc', true);
        } else {
            $ionicPopup.alert({
                template: 'Escape Unsuccessful',
                cssClass: 'adminpop'
            });

            store.set('isinBattleEsc', false);
            $scope.canescape = false;
        }
    }

    var redo = true;


    setInterval(function() {
        var myCreaturesRef = firebase.database().ref().child("Codes/").child($scope.username)
        var myCreaturesArray = $firebaseArray(myCreaturesRef);
        var miniCreatureArray = [];


        myCreaturesArray.$loaded(function(x) {
            angular.forEach(myCreaturesArray, function(val) {
                if (val.favourite == true) {
                    miniCreatureArray.push({
                        name: val.Monster,
                        image: val.MonsterImg,
                        CustomCss: val.CustomCss,
                        shiny: val.Shiny,
                        lvl: val.Monsterlvl,
                        env: val.MonsterEnviroment,
                        rare: val.MonsterRarity,
                        trait: val.Traites,
                        barcode: val.barcode
                    });
                }
            });
        });

        $scope.creatures = miniCreatureArray;
    }, 3000);




    $scope.showMoreCInfo = function(creature) {

        console.log(creature);

        $scope.creatureImage = creature.image;
        $scope.CustomCss = creature.CustomCss;
        $scope.Shiny = creature.shiny;
        $scope.creaturezoomName = creature.name;
        $scope.creaturezoomLvl = creature.lvl;
        $scope.creaturezoomEnviroment = creature.env;
        $scope.creaturezoomRarity = creature.rare;
        $scope.creatureTraits = creature.trait
        $scope.creaturezoomCode = creature.barcode

    }

    $scope.battleState = function(creaturezoomName, creatureImage, CustomCss, Shiny, creaturezoomLvl, creaturezoomRarity, creaturezoomCode, $state) {

        var playermode = store.get('playermode');

        store.set('playermode', 2);

        $scope.playermode = 2;

        var acreaturezoomName = store.get('creatureName');

        if (!acreaturezoomName) {
            console.log("CREATURE PICKED")


            var teHealth = creaturezoomLvl * 50;
            store.set('hp', teHealth);
            $scope.myHealth = teHealth;
            $scope.maxhp = teHealth;

            console.log("MY HP SET TO " + teHealth)

            var atk = Math.floor(Math.random() * ((creaturezoomLvl*10) - (creaturezoomLvl/2) + 1) + (creaturezoomLvl/2));
            store.set('myatk', atk);
            $scope.myatk = atk;

            var def = Math.floor(Math.random() * ((creaturezoomLvl*10) - (creaturezoomLvl/2) + 1) + (creaturezoomLvl/2));
            store.set('mydef', def);
            $scope.mydef = def;

            var stn = Math.floor(Math.random() * ((creaturezoomLvl*10) - (creaturezoomLvl/2) + 1) + (creaturezoomLvl/2));
            store.set('mystn', stn);
            $scope.mystn = stn;

            store.set('creatureName', creaturezoomName);
            store.set('creatureImage', creatureImage);
            store.set('creatureCustomCss', CustomCss);
            store.set('creatureShiny', Shiny);
            store.set('creatureLvl', creaturezoomLvl);
            store.set('creatureRarity', creaturezoomRarity);
            store.set('creatureCode', creaturezoomCode);
        }

        var acreaturezoomName = store.get('creatureName');
        var acreatureImage = store.get('creatureImage');
        var acreatureCustomCss = store.get('creatureCustomCss');
        var acreatureShiny = store.get('creatureShiny');
        var acreatureLvl = store.get('creatureLvl');
        var acreatureRarity = store.get('creatureRarity');
        var acreatureCode = store.get('creatureCode');


        $scope.creatureName = acreaturezoomName
        $scope.creatureImage = acreatureImage,
        $scope.CustomCss = acreatureCustomCss,
        $scope.Shiny = acreatureShiny,
        $scope.creatureLvl = acreatureLvl,
        $scope.creatureRarity = acreatureRarity,
        $scope.creatureCode = acreatureCode

        if (acreatureCode) {
            var ctraiteref = firebase.database().ref().child("Codes").child($scope.username).child(acreatureCode).child("Traites")
            $scope.cTraites = $firebaseArray(ctraiteref)
            console.log($scope.cTraites)
        }
    }



    var acreaturezoomName = store.get('creatureName');
    var acreatureImage = store.get('creatureImage');
    var acreatureCustomCss = store.get('creatureCustomCss');
    var acreatureShiny = store.get('creatureShiny');
    var acreatureLvl = store.get('creatureLvl');
    var acreatureRarity = store.get('creatureRarity');
    var acreatureCode = store.get('creatureCode');


    $scope.creatureName = acreaturezoomName
    $scope.creatureImage = acreatureImage,
    $scope.CustomCss = acreatureCustomCss,
    $scope.Shiny = acreatureShiny,
    $scope.creatureLvl = acreatureLvl,
    $scope.creatureRarity = acreatureRarity,
    $scope.creatureCode = acreatureCode

    if (acreatureCode) {
        var ctraiteref = firebase.database().ref().child("Codes").child($scope.username).child(acreatureCode).child("Traites")
        $scope.cTraites = $firebaseArray(ctraiteref)
        console.log($scope.cTraites)
    }


    $scope.applymodifier = function(cTraite) {
        console.log("Apply Modifiers")

        $scope.mytraiten = cTraite.Name;
        $scope.mytraitede = cTraite.Desc;
        $scope.mytraitea = cTraite.Attack;
        $scope.mytraited = cTraite.Defense;
        $scope.mytraites = cTraite.Stun;

        if (cTraite.Attack && $scope.myatk < 2000) {
            $scope.myatk = store.get('myatk');
            $scope.mynatk = +$scope.myatk + +cTraite.Attack
            store.set('myatk', $scope.mynatk);
            console.log("You attack is at " + $scope.mynatk)
        }

        if (cTraite.Defense && $scope.mydef < 2000) {
            $scope.mydef = store.get('mydef');

            $scope.myndef = +$scope.mydef + +cTraite.Defense
            store.set('mydef', $scope.myndef);
            console.log("You Defense is at " + $scope.myndef + " Which is " + $scope.mydef + " + " + cTraite.Defense)
        }

        if (cTraite.Stun && $scope.mystn < 2000) {
            $scope.mystn = store.get('mystn');
            $scope.mynstn = +$scope.mystn + +cTraite.Stun
            store.set('mystn', $scope.mynstn);
            console.log("You Stun is at " + $scope.mynstn)
        }

        $scope.aimove = true;

        $timeout(function() {
            $scope.aiMoves()

            $scope.eatk = store.get('eatk');
            $scope.edef = store.get('edef');
            $scope.estn = store.get('estn');

            $scope.myatk = store.get('myatk');
            $scope.mydef = store.get('mydef');
            $scope.mystn = store.get('mystn');

            $scope.totalHealth = store.get('enemyhp');
            $scope.myHealth = store.get('hp');
            console.log($scope.totalHealth)


        }, 2500);


    }

    $scope.aiMoves = function() {
        console.log("Generate AI Moves")

        var allTraitesRef = firebase.database().ref().child("Traites")
        var cTraites = $firebaseArray(allTraitesRef);


        cTraites.$loaded(function() {

          $scope.movemessage = true;

          var picktraite = Math.floor(Math.random() * (($scope.cTraites.length - 1) - 0)) + 0;
          var eTraite = cTraites[picktraite];

          console.log(picktraite)
          console.log(cTraites)

          $scope.etraiten = eTraite.Name;
          $scope.etraitede = eTraite.Desc;
          $scope.etraitea = eTraite.Attack;
          $scope.etraited = eTraite.Defense;
          $scope.etraites = eTraite.Stun;


          var attak = store.get('eatk');
          var attakt = +attak + +$scope.etraitea
          store.set('eatk', attakt);
          $scope.eatk = attakt;
          console.log("Enemy attack is at " + attakt)

          var defense = store.get('edef');
          var defenset = +defense + +$scope.etraited
          store.set('edef', defenset);
          $scope.edef = defenset;
          console.log("Enemy defense is at " + defenset)

          var stun = store.get('estn');
          var stunt = +stun + +$scope.etraites
          store.set('estn', stunt);
          $scope.estn = stunt;
          console.log("Enemy stun is at " + stunt)
        });


        $timeout(function() {
            $scope.applyMoves();
        }, 3000);
    }



    $scope.applyMoves = function() {
        console.log("Applying Moves")

        $scope.movemessage = false;

        $scope.eatk = store.get('eatk');
        $scope.edef = store.get('edef');

        $scope.mydef = store.get('mydef');
        $scope.myatk = store.get('myatk');

        $scope.enemyHealth = store.get('enemyhp');
        $scope.myHealth = store.get('hp');

        $scope.creatureLvl = store.get('creatureLvl');
        $scope.Level = store.get('isinBattlelvl');

        var myNewHealth = $scope.myHealth - $scope.eatk;
        var enemyNewHealth = $scope.enemyHealth - $scope.myatk;

        var myNewShield = $scope.mydef - $scope.eatk;
        var enemyNewShield = $scope.edef - $scope.myatk;

        console.log("MY DEFENSE IS " + $scope.mydef)
        console.log("MY NEW DEFENSE IS " + myNewShield)

        console.log("MY HEALTH IS " + $scope.myHealth)
        console.log("MY NEW HEALTH IS " + myNewHealth)

        if(myNewShield <= 0){
          if(myNewHealth <= 0){
            console.log("IM DEAD " + myNewHealth)
            $scope.MeDefeated();
          }else{
            console.log("DAMAGE DELT TO MY HEALTH")

            var myNewHealthNewer = myNewHealth - $scope.mydef
            store.set('hp', myNewHealthNewer);
            $scope.myHealth = myNewHealthNewer;
            console.log("MY HEALTH AT " + myNewHealthNewer)

            $scope.mydef = 0;
            store.set('mydef', 0);
          }
        }else if (enemyNewShield <= 0){
          if(enemyNewHealth <= 0){
            console.log("ENEMY DEAD " + enemyNewHealth)
            $scope.EnemyDefeated();
          }else{
            console.log("DAMAGE DELT TO ENEMY HEALTH")

            if($scope.mydef <= 0){
              var enemyNewHealthNewer = enemyNewHealth - $scope.edef
              store.set('enemyhp', enemyNewHealthNewer);
              $scope.totalHealth = enemyNewHealthNewer;
              console.log("ENEMY HEALTH AT " + enemyNewHealthNewer)

              store.set('edef', 0);
              $scope.edef = 0;
            }else{
              store.set('enemyhp', enemyNewHealth);
              $scope.totalHealth = enemyNewHealth;
              console.log("ENEMY HEALTH AT " + enemyNewHealth)
            }
          }
        }else{
            console.log("DAMAGE DELT TO ENEMY SHIELDS")

            store.set('edef', enemyNewShield);
            $scope.edef = enemyNewShield;

            console.log("ENEMY SHIELD AT " + enemyNewShield)

            console.log("DAMAGE DELT TO MY SHIELDS")

            store.set('mydef', myNewShield);
            $scope.mydef = myNewShield;

            console.log("MY SHIELD AT " + myNewShield)
        }




        $timeout(function() {
            console.log("Done Move")
            $scope.movemessage = false;
            $scope.aimove = false;
        }, 500);
    }




    $scope.MeDefeated = function() {
      var monterandom = Math.floor(Math.random() * ($scope.creatureLvl - 5 + 1) + 5) * 20;
      var nyname = store.set('creatureName', 0);

      $ionicPopup.alert({
          template: 'Oh no, you have lost. You lost ' + monterandom + " Montae",
          cssClass: 'adminpop animated tada',
      });

      var ref = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/montae");
      var montaeref = new $firebaseObject(ref);

      montaeref.$loaded(function() {
          montaeref.$value = montaeref.$value - monterandom;
          montaeref.$save();
      });

      $ionicHistory.clearCache().then(function() {
          $state.go('tab.planet');
      });

      store.set('playermode', 1);

      store.set('enemyhp', 0);
      store.set('hp', 0);

      store.set('eatk', 0);
      store.set('edef', 0);
      store.set('estn', 0);
      store.set('myatk', 0);
      store.set('mydef', 0);
      store.set('mystn', 0);

      store.set('creatureName', 0);
      store.set('creatureImage', 0);
      store.set('creatureCustomCss', 0);
      store.set('creatureShiny', 0);
      store.set('creatureLvl', 0);
      store.set('creatureRarity', 0);
      store.set('creatureCode', 0);

      store.set('isinBattle', false);
      store.set('isinBattlelvl', 0);
      store.set('isinBattleEsc', false);
    }


    $scope.EnemyDefeated = function() {
        var monterandom = Math.floor(Math.random() * ($scope.Level - 5 + 1) + 5) * 20;

        var nyname = store.set('creatureName', 0);

        $ionicPopup.alert({
         template: 'Congratulations, you won! You gained ' + monterandom + " Montae",
         cssClass: 'adminpop animated tada',
        });

        var ref = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/montae");
        var montaeref = new $firebaseObject(ref);

        montaeref.$loaded(function() {
         montaeref.$value = montaeref.$value + monterandom;
         montaeref.$save();
        });

        $ionicHistory.clearCache().then(function() {
         $state.go('tab.planet');
        });

        store.set('playermode', 1);

        store.set('enemyhp', 0);
        store.set('hp', 0);

        store.set('eatk', 0);
        store.set('edef', 0);
        store.set('estn', 0);
        store.set('myatk', 0);
        store.set('mydef', 0);
        store.set('mystn', 0);

        store.set('creatureName', 0);
        store.set('creatureImage', 0);
        store.set('creatureCustomCss', 0);
        store.set('creatureShiny', 0);
        store.set('creatureLvl', 0);
        store.set('creatureRarity', 0);
        store.set('creatureCode', 0);

        store.set('isinBattle', false);
        store.set('isinBattlelvl', 0);
        store.set('isinBattleEsc', false);
    }

});
