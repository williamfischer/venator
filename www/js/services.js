angular.module('starter.services', [])

.factory("Auth", function($firebaseAuth) {
  return $firebaseAuth();
})

.factory("Codes", function($firebaseArray) {
  var codesRef = firebase.database().ref().child("Codes");
  return $firebaseArray(codesRef);
})

.factory('Monsters', function($firebaseArray) {
  var monstersRef = firebase.database().ref().child("Monsters");
  return $firebaseArray(monstersRef);
})

.factory('Technology', function($firebaseArray) {
  var technologyRef = firebase.database().ref().child("Technology");
  return $firebaseArray(technologyRef);
})

.factory('Friends', function($firebaseArray) {
  var friendsRef = firebase.database().ref().child("Codes");
  return $firebaseArray(friendsRef);
})

.factory("Bugs", function($firebaseArray) {
  var bugsRef = firebase.database().ref().child("Bugs");
  return $firebaseArray(bugsRef);
})


.directive('imageonload', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.bind('load', function() {
                    scope.$apply(attrs.imageonload)(true);
                });
                element.bind('error', function(){
                  scope.$apply(attrs.imageonload)(false);
                });
            }
        };
    })

.directive('particlesDrv', function($window, $log){
  return {
    restrict: 'A',
    template: '<div class="particleJs" id="particleJs"></div>',
    link: function(scope, element, attrs, fn) {
      $log.debug('test');
      $window.particlesJS('particleJs', {
        particles: {
          number: {
            value: 208,
            density: {
              enable: true,
              value_area: 800
            }
          },
          color: {
            value: '#22313F'
          },
          shape: {
            type: "circle",
            stroke: {
              width: 0,
              color: "#af0000"
            },
            polygon: {
              nb_sides: 9
            }
          },
          opacity: {
            value: 0.37,
            random: false,
            anim: {
              enable: false,
              speed: 0.1,
              opacity_min: 0.1,
              sync: false
            }
          },
          size: {
            value: 1.5,
            random: true,
            anim: {
              enable: false,
              speed: 40,
              size_min: 0.1,
              sync: false
            }
          },
          line_linked: {
            enable: false,
            distance: 150,
            color: '#fff',
            opacity: 0.4,
            width: 1
          },
          move: {
            enable: true,
            speed: 1,
            direction: 'none',
            random: true,
            straight: false,
            out_mode: 'bounce',
            bounce: false,
            attract: {
              enable: false,
              rotateX: 600,
              rotateY: 1200
            }
          }
        },
        interactivity: {
          detect_on: 'canvas',
          events: {
            onhover: {
              enable: false,
              mode: 'repulse'
            },
            onclick: {
              enable: true,
              mode: 'repulse'
            },
            resize: true
          },
          modes: {
            repulse: {
              distance: 103,
              duration: 0.4
            }
          }
        },
        retina_detect: true
      });
    }
  };
})

.directive('countdown', [
    'Util', '$interval', function(Util, $interval) {
      return {
        restrict: 'A',
        scope: {
          date: '@'
        },
        link: function(scope, element) {
          var future;
          future = new Date(scope.date);
          $interval(function() {
            var diff;
            diff = Math.floor((future.getTime() - new Date().getTime()) / 1000);
            return element.text(Util.dhms(diff));
          }, 1000);
        }
      };
    }
  ])

.factory('Util', [
    function() {
      return {
        dhms: function(t) {
          var days, hours, minutes, seconds;
          days = Math.floor(t / 86400);
          t -= days * 86400;
          hours = Math.floor(t / 3600) % 24;
          t -= hours * 3600;
          minutes = Math.floor(t / 60) % 60;
          t -= minutes * 60;
          seconds = t % 60;
          return [days + 'd', hours + 'h', minutes + 'm', seconds + 's'].join(' ');
        }
      };
    }
  ]);
