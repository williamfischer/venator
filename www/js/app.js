angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ngCordova', 'firebase', 'angular-storage', 'ngAnimate', 'angular-toArrayFilter'])

.run(function($ionicPlatform, $state, Auth, $firebaseObject, $ionicPopup, store) {

    $ionicPlatform.ready(function() {

        var pwuser = store.get('pwid');
        var fbuser = store.get('fbid');

        if (pwuser) {
            console.log("Pwuser" + pwuser)
            var username = pwuser;
            if (username === null) {
                $state.go('login')
            } else {
                var codesRef = firebase.database().ref().child("Codes");
                var usersRef = codesRef.child(username);
                var infoRef = usersRef.child('User Information/SystemMessage');

                var loadedobj = new $firebaseObject(infoRef);

                loadedobj.$watch(function() {

                    loadedobj.$loaded().then(function() {

                        if (loadedobj.$value === false) {} else {
                            var alertPopup = $ionicPopup.alert({
                                template: loadedobj.$value,
                                cssClass: 'adminpop'
                            });

                            alertPopup.then(function(res) {
                                loadedobj.$value = false;
                                loadedobj.$save();
                            });
                        }
                    });

                });

                var infoRef2 = usersRef.child('User Information');
                var final2ref = infoRef2.child('id')
            }
          $state.go('tab.home')
        } else if (fbuser) {
            var username = fbuser;
            if (username === null) {
                $state.go('login')
            } else {
                var ref = firebase.database().ref().child("https://somethingodd.firebaseio.com");
                var codesRef = ref.child("Codes");
                var usersRef = codesRef.child(username);
                var infoRef = usersRef.child('User Information/SystemMessage');

                var loadedobj = new $firebaseObject(infoRef);

                loadedobj.$watch(function() {

                    loadedobj.$loaded().then(function() {

                        if (loadedobj.$value === false) {} else {
                            var alertPopup = $ionicPopup.alert({
                                template: loadedobj.$value,
                                cssClass: 'adminpop'
                            });

                            alertPopup.then(function(res) {
                                loadedobj.$value = false;
                                loadedobj.$save();
                            });
                        }
                    });

                });

                var infoRef2 = usersRef.child('User Information');
                var final2ref = infoRef2.child('id')
            }

          $state.go('tab.home')

        } else {
          $state.go('login')
        }


        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            // cordova.plugins.Keyboard.disableScroll(true);

            var permissions = cordova.plugins.permissions;

            window.AndroidFullScreen.immersiveMode();

            permissions.hasPermission(permissions.CAMERA, checkPermissionCallback, null);

            function checkPermissionCallback(status) {
                if (!status.hasPermission) {
                    var errorCallback = function() {
                        console.warn('Camera permission is not turned on');
                    }

                    permissions.requestPermission(
                        permissions.CAMERA,
                        function(status) {
                            if (!status.hasPermission) errorCallback();
                        },
                        errorCallback);
                }
            }
        }
        if (window.StatusBar) {
            StatusBar.styleBlackOpaque();
        }

        if (window.navigator && window.navigator.splashscreen) {
            window.plugins.orientationLock.unlock();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom');
    // $ionicConfigProvider.navBar.alignTitle('center').positionPrimaryButtons('left');

    var config = {
        apiKey: "AIzaSyC4agkJEniBhaFQDDkLw4S1W5eO7u4jUCg",
        authDomain: "somethingodd.firebaseapp.com",
        databaseURL: "https://somethingodd.firebaseio.com",
        projectId: "firebase-somethingodd",
        storageBucket: "firebase-somethingodd.appspot.com"
    };
    firebase.initializeApp(config);

    $stateProvider

    // setup an abstract state for the tabs directive
        .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
    })

    .state('tab.home', {
        url: '/home',
        views: {
            'tab-home': {
                templateUrl: 'templates/home.html',
                controller: 'HomeCtrl'
            }
        }
    })

    .state('tab.planet', {
        url: '/planet',
        views: {
            'tab-planet': {
                templateUrl: 'templates/planet.html',
                controller: 'PlanetCtrl'
            }
        }
    })

    .state('tab.planetWalk', {
        url: '/planet/planetWalk',
        views: {
            'tab-planet': {
                templateUrl: 'templates/planetWalk.html',
                controller: 'planetWalkCtrl'
            }
        }
    })

    .state('tab.battleAI', {
        url: '/planet/battleAI',
        views: {
            'tab-planet': {
                templateUrl: 'templates/battleAI.html',
                controller: 'battleAICtrl'
            }
        }
    })


    .state('tab.barcode', {
        url: '/monsters/barcode',
        views: {
            'tab-monsters': {
                templateUrl: 'templates/barcode.html',
                controller: 'BarcodeCtrl'
            }
        }
    })

    .state('tab.events', {
        url: '/monsters/events',
        views: {
            'tab-monsters': {
                templateUrl: 'templates/events.html',
                controller: 'EventsCtrl'
            }
        }
    })

    .state('tab.monsters', {
        url: '/monsters',
        views: {
            'tab-monsters': {
                templateUrl: 'templates/monsters.html',
                controller: 'MonstersCtrl'
            }
        }
    })

    .state('tab.droplets', {
        url: '/monsters/droplets',
        views: {
            'tab-monsters': {
                templateUrl: 'templates/droplets.html',
                controller: 'DropletsCtrl'
            }
        }
    })

    .state('tab.dreamer', {
        url: '/dreamer',
        views: {
            'tab-dreamer': {
                templateUrl: 'templates/dreamer.html',
                controller: 'DreamerCtrl'
            }
        }
    })

    .state('tab.friend', {
        url: '/friend',
        views: {
            'tab-friend': {
                templateUrl: 'templates/friend.html',
                controller: 'FriendCtrl'
            }
        }
    })

    .state('tab.settings', {
        url: 'home/settings',
        views: {
            'tab-home': {
                templateUrl: 'templates/settings.html',
                controller: 'SettingsCtrl'
            }
        }
    })

    .state('tab.about', {
        url: '/home/settings/about',
        views: {
            'tab-home': {
                templateUrl: 'templates/about.html',
                controller: 'AboutCtrl'
            }
        }
    })

    .state('tab.bugger', {
        url: '/bugger',
        views: {
            'tab-home': {
                templateUrl: 'templates/bugger.html',
                controller: 'BuggerCtrl'
            }
        }
    });

    $urlRouterProvider.otherwise('/login');

});
