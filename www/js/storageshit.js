$scope.applyMoves = function() {
    $scope.aimove = true;

    $scope.timetaken = 0;

    $scope.edef = store.get('edef');
    $scope.mydef = store.get('mydef');
    $scope.myatk = store.get('myatk');
    $scope.eatk = store.get('eatk');
    $scope.totalHealth = store.get('enemyhp');
    $scope.myHealth = store.get('hp');
    $scope.creatureLvl = store.get('creatureLvl');
    $scope.Level = store.get('isinBattlelvl');
    var nyname = store.get('creatureName');

    // MY HANDLER

    console.log("My defense at " + ($scope.mydef - $scope.eatk))

    if(($scope.eatk - $scope.mydef) <=0){
        if (($scope.myHealth + $scope.eatk) <= 0) {
            console.log("I HAVE BEEN DEFEATED")

            store.set('hp', 0);
            $scope.myHealth = store.get('hp')
        }else{
            console.log("I HAVE LOST " + $scope.eatk + " HEALTH. HEALTH AT " + ($scope.myHealth - $scope.eatk))

            store.set('hp', ($scope.myHealth - $scope.eatk));
            $scope.myHealth = store.get('hp')
        }
    }else{
        console.log("I HAVE TAKEN SHIELD DAMAGE")

        $timeout(function() {
            $scope.movemessage = "YOUR SHIELD HAS TAKEN " + $scope.eatk + " DAMAGE. SHIELDS AT " + ($scope.mydef - $scope.eatk);
        }, 2000);

        store.set('mydef', ($scope.eatk - $scope.mydef));
        $scope.mydef = store.get('hp')
    }
    // if ($scope.mydef <= 0) {
    //     if ($scope.myHealth <= 0) {
    //         var monterandom = Math.floor(Math.random() * ($scope.creatureLvl - 5 + 1) + 5) * 20;

    //         var nyname = store.set('creatureName', 0);

    //         $ionicPopup.alert({
    //             template: 'Oh no, you have lost. You lost ' + monterandom + " Montae",
    //             cssClass: 'adminpop animated tada',
    //         });

    //         var ref = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/montae");
    //         var montaeref = new $firebaseObject(ref);

    //         montaeref.$loaded(function() {
    //             montaeref.$value = montaeref.$value - monterandom;
    //             montaeref.$save();
    //         });

    //         $ionicHistory.clearCache().then(function() {
    //             $state.go('tab.planet');
    //         });

    //         store.set('playermode', 1);

    //         store.set('enemyhp', 0);
    //         store.set('hp', 0);

    //         store.set('eatk', 0);
    //         store.set('edef', 0);
    //         store.set('estn', 0);
    //         store.set('myatk', 0);
    //         store.set('mydef', 0);
    //         store.set('mystn', 0);

    //         store.set('creatureName', 0);
    //         store.set('creatureImage', 0);
    //         store.set('creatureCustomCss', 0);
    //         store.set('creatureShiny', 0);
    //         store.set('creatureLvl', 0);
    //         store.set('creatureRarity', 0);
    //         store.set('creatureCode', 0);

    //         store.set('isinBattle', false);
    //         store.set('isinBattlelvl', 0);
    //         store.set('isinBattleEsc', false);
    //     } else {
    //         var newmHealth = $scope.myHealth - $scope.eatk;

    //         if(!$scope.timetaken){
    //             $scope.timetaken = 1000;
    //         }else{
    //             $scope.timetaken = $scope.timetaken + 1000;
    //         }

    //         console.log("YOU LOSE " + $scope.eatk + " HP, HEALTH AT " + newmHealth)
    //         $scope.movemessage = "YOU LOSE " + $scope.eatk + " HP, HEALTH AT " + newmHealth

    //         store.set('hp', newmHealth);

    //         $scope.myHealth = store.get('hp')
    //     }
    // } else {
    //     if(!$scope.timetaken){
    //         $scope.timetaken = 1000;
    //     }else{
    //         $scope.timetaken = $scope.timetaken + 1000;
    //     }

    //     var newmDefense = $scope.mydef - $scope.eatk;

    //     if(newmDefense <= 0){
    //         $scope.movemessage = "SHIELD BROKEN. YOU LOSE " + $scope.eatk + " DEFENSE, DEFENSE AT " + newmDefense
    //         console.log("SHIELD BROKEN. YOU LOSE " + $scope.eatk + " DEFENSE, DEFENSE AT " + newmDefense)
    //     }else{
    //         $scope.movemessage = "YOU LOSE " + $scope.eatk + " DEFENSE, DEFENSE AT " + newmDefense
    //         console.log("YOU LOSE " + $scope.eatk + " DEFENSE, DEFENSE AT " + newmDefense)
    //     }

    //     console.log(newmDefense)
    //     store.set('mydef', newmDefense);

    //     $scope.mydef = store.get('mydef');
    // }

    // ENEMY HANDLER
    if(($scope.edef - $scope.myatk)  <= 0){
        if(($scope.myatk + $scope.totalHealth) <= 0){
            console.log("ENEMY DESTROYED")

            $timeout(function() {
                $scope.movemessage = "ENEMY DEFEATED"
            }, 2000);

            $scope.EnemyDefeated()
        }else{
            console.log('ATTACK HEALTH')

            $timeout(function() {
                $scope.movemessage = "ENEMY HEALTH HAS BEEN REDUCED BY " + $scope.myatk + " ENEMY HAS " + ($scope.myatk + $scope.totalHealth) + " HEALTH"
            }, 2000);

            var neweHealth = $scope.totalHealth - $scope.myatk;
            store.set('enemyhp', neweHealth);
            $scope.totalHealth = store.get('enemyhp');
            $scope.enemyHealthChange = $scope.myatk;

            if(($scope.myatk + $scope.totalHealth) <= 0){
                console.log("ENEMY DESTROYED")

                $timeout(function() {
                    $scope.movemessage = "ENEMY DEFEATED"
                }, 2000);

                $scope.EnemyDefeated()
            }
        }
    }else{
        console.log('ENEMY\'S SHIELD DAMAGED');

        $timeout(function() {
            $scope.movemessage = "ENEMY LOSES " + ($scope.edef - $scope.myatk) + " DEFENSE"
        }, 2000);

        var neweDefense = $scope.edef - $scope.myatk;
        store.set('edef', neweDefense);

        $scope.edef = store.get('edef')
    }

    $scope.movemessage = false;
    store.set('playermode', 2);


    // if (($scope.edef + $scope.myatk)  <= 0) {
    //     if ($scope.totalHealth <= 0) {
    //         var monterandom = Math.floor(Math.random() * ($scope.Level - 5 + 1) + 5) * 20;

    //         var nyname = store.set('creatureName', 0);

    //         $ionicPopup.alert({
    //             template: 'Congratulations, you won! You gained ' + monterandom + " Montae",
    //             cssClass: 'adminpop animated tada',
    //         });

    //         var ref = firebase.database().ref().child("Codes/" + $scope.username + "/User Information/montae");
    //         var montaeref = new $firebaseObject(ref);

    //         montaeref.$loaded(function() {
    //             montaeref.$value = montaeref.$value + monterandom;
    //             montaeref.$save();
    //         });

    //         $ionicHistory.clearCache().then(function() {
    //             $state.go('tab.planet');
    //         });

    //         store.set('playermode', 1);

    //         store.set('enemyhp', 0);
    //         store.set('hp', 0);

    //         store.set('eatk', 0);
    //         store.set('edef', 0);
    //         store.set('estn', 0);
    //         store.set('myatk', 0);
    //         store.set('mydef', 0);
    //         store.set('mystn', 0);

    //         store.set('creatureName', 0);
    //         store.set('creatureImage', 0);
    //         store.set('creatureCustomCss', 0);
    //         store.set('creatureShiny', 0);
    //         store.set('creatureLvl', 0);
    //         store.set('creatureRarity', 0);
    //         store.set('creatureCode', 0);

    //         store.set('isinBattle', false);
    //         store.set('isinBattlelvl', 0);
    //         store.set('isinBattleEsc', false);
    //     } else {
    //         var neweHealth = $scope.totalHealth - $scope.myatk;
    //         console.log("ENEMY LOSES " + $scope.myatk + " HP, HEALTH AT " + neweHealth)

    //         $scope.timetaken = $scope.timetaken + 1000;
    //         $scope.movemessage = "ENEMY LOSES " + $scope.myatk + " HP, HEALTH AT " + neweHealth

    //         store.set('enemyhp', neweHealth);

    //         $scope.totalHealth = store.get('enemyhp');
    //         $scope.enemyHealthChange = $scope.myatk;
    //     }
    // } else {
    //     console.log("ENEMY LOSES " + $scope.eatk + " DEFENSE");

    //     $scope.timetaken = $scope.timetaken + 1000;
    //     $scope.movemessage = "ENEMY LOSES " + $scope.eatk + " DEFENSE"

    //     var neweDefense = $scope.edef - $scope.myatk;
    //     store.set('edef', neweDefense);

    //     $scope.edef = store.get('edef');
    // }



}
